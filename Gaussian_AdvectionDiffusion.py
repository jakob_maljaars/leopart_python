"""
    Script for simulating the advection-diffusion of a rotating Gaussian pulse
    on a circular domain.
"""

from dolfin import *
from mshr import *
from mpi4py import MPI as pyMPI
import time as tm
##
from LEoPart import *
from StaticCondensation import FormsDiffusion, Diffusion_StaticCondensation
import numpy as np 
import os

comm = pyMPI.COMM_WORLD

# Input block
outdir_base = './Results/Gaussian_AdvectionDiffusion/'
# Geometric properties
nx_list = [8, 16, 32, 64] 
x0,y0   = 0., 0.        # Center of domain
xc,yc   = -0.15, 0.     # Center of Gaussian
r       = .5

# Polynomial orders
k_list          = [2, 1, 2, 2] 
l_list          = [0]* (len(k_list)-1) + [1] 
kbar_list       = k_list

# Apply consistency term in PDE-map?
# If False --> \theta_L in PDE = 1.0 if True, then \theta_L = 0.5
consistency_term = [True, True, True, True]

# Advective velocity
Uh_list = [0., np.pi, np.pi, np.pi]

# Particle resolution
pres= [100, 200, 400, 800]
# Avoid particle placement outside disk-shaped region
eps = [5e-3, 2.5e-3, 1.25e-3, 0.625e-3] 

# Time-stepping
dt_list         = [Constant(0.08), Constant(0.04), Constant(0.02), Constant(0.01)] 
Tend            = 2.0 

# Theta value for particle-mesh projection. Note that theta value not relevant if l = 0!
thetaPDE_list   = [Constant(0.)] * (len(k_list) - 1 ) + [Constant(0.5)] 

# Theta value for particle update:
thetaP_list     = [Constant(0.5)] * len(k_list) 
# Theta value for Diffusion step: Crank-Nicolson --> \theta = 0.5, Backward Euler --> 1.0
thetaDiff_list  = [Constant(1.0)] * len(k_list) 

# Beta in PDE constrained (set to default)
#beta            = Constant(1e-6)

# Body force and initial condition
f         = Constant(0.)
sigma     = Constant(0.1)
kappa_list= [Constant(1E-2), Constant(1E-3), Constant(0.)]

phi_exact = '2*pow(sigma,2) / (2*pow(sigma,2) + 4 * kappa *t)' \
            '* exp( -(pow(x[0]*cos(Uh*t) + x[1]*sin(Uh*t) - x0,2)+pow(-x[0]*sin(Uh*t) + x[1]*cos(Uh*t)-y0,2))' \
            '/(2*pow(sigma,2) + 4 * kappa *t))'

# Function to initialize particle values
def assign_particle_values(x, phi_exact):
    if comm.Get_rank() == 0:
        s=np.asarray([phi_exact(x[i,:]) for i in range(len(x))], dtype = np.float_)
    else:
        s = None
    return s

for i,(k,l,kbar) in enumerate(zip(k_list, l_list, kbar_list)):
    # Set penalty parameter
    alpha           = Constant(12.*k*k)
    
    for kappa in kappa_list:
        outdir      = outdir_base+'kappa_'+str(float(kappa))+'/Case_k'+str(k)+'l'+str(l)+'kbar' \
                        +str(kbar)+'_case'+str(i+1)+'/'
        error_table = outdir+'error_table'
        
        try:
            os.remove(error_table)
        except OSError:
            pass
        
        # Skip computation when trivial (i.e. a = 0 and kappa = 0)
        if Uh_list[i] == 0. and float(kappa) == 0.: 
            continue

        for j,nx in enumerate(nx_list):
            if comm.Get_rank() == 0:
                print("Starting computation with grid resolution "+str(nx))
            
            dt        = dt_list[j]
            num_steps = np.rint(Tend/float(dt))
            
            uh        = Expression(('-Uh*x[1]','Uh*x[0]'),Uh = Uh_list[i], degree=3) 
            phi_expr  = Expression(phi_exact, degree = 6, Uh = Uh_list[i], x0=float(xc), 
                                y0 = float(yc), sigma=float(sigma), t = 0., kappa = float(kappa))
        
            # Generate mesh
            domain = Circle(Point(x0,y0),r,nx*4)
            mesh = generate_mesh(domain,nx)
                         
            # Create function spaces: advection part
            W       = FunctionSpace(mesh, 'DG', k)
            T       = FunctionSpace(mesh, 'DG', l)
            Wbar    = FunctionSpace(mesh, 'DGT',kbar)         
            Wbard   = Wbar
                                            
            phih, phih_star, phih0  = Function(W), Function(W), Function(W)
            psih0                   = Function(W)
            dphi0, dphi00           = Function(W), Function(W)
            
            lambh                   = Function(T)
            psibarh                 = Function(Wbar)
            phibarh, phibarh0       = Function(Wbard), Function(Wbard)
            
            # Initial condition 
            psih0.assign(phi_expr); phih0.assign(phi_expr)   
            
            # Initialize PDE constrained map
            FuncSpace_adv  = {'FuncSpace_local': W, 'FuncSpace_lambda': T , 'FuncSpace_bar': Wbar}
            FuncSpace_diff = {'FuncSpace_local': W, 'FuncSpace_bar': Wbard}
             
            # Set theta_L to 1 in first step 
            theta_L        = Constant(1.0)          
            forms_adv      = FormsPDEMap(mesh, FuncSpace_adv).forms_theta_linear(psih0, uh, dt, thetaPDE_list[i],
                                                                                 theta_L, dphi0, dphi00)                                                                                
            forms_diff     = FormsDiffusion(mesh, FuncSpace_diff).forms_theta(phih_star, phih0, phibarh0, dt,
                                                                               kappa, alpha, f,  thetaDiff_list[i])
            
            pde_projection = PDEMap(mesh, forms_adv['N_a'], forms_adv['G_a'], forms_adv['L_a'],
                                                                              forms_adv['H_a'], 
                                                                              forms_adv['B_a'],
                                          forms_adv['Q_a'], forms_adv['R_a'], forms_adv['S_a'])                        
            diff_sc        = Diffusion_StaticCondensation(mesh, forms_diff['A_d'],   forms_diff['G_d'],
                                                                forms_diff['G_T_d'], forms_diff['B_d'], 
                                                                forms_diff['F_d'],   forms_diff['H_d'])
            
            # Boundary conditions and initial condition
            bc_a = DirichletBC(Wbar, phi_expr, 'on_boundary') 
            bc_d = DirichletBC(Wbard, phi_expr, 'on_boundary')
            diff_sc.assemble([bc_d])
            
            # Define particles
            x    = RandomCircle(Point(x0, y0), r-eps[j], seed = 20).generate([pres[j], pres[j]])
            s    = assign_particle_values(x, phi_expr)
            x, s = comm.bcast(x, root=0), comm.bcast(s, root=0)
            
            # Initialize particles 
            Vadv    = VectorFunctionSpace(mesh, 'DG', 5)
            uadvect = interpolate(uh,Vadv)
            lp      = LagrangianParticles(Vadv, 'RK3')
            lp.add_particles(x, properties_d = {'Scalar': s, 'deltaScalar': [None]*len(x) })
            
            # Set initial particle values
            lp.mesh_to_particles(psih0,'Scalar')
           
            # Compute area at t = 0
            area_0 = assemble(phih0*dx)
            
            # Time stepping loop
            step = 0
            while step < num_steps:
                step += 1
                if comm.Get_rank() == 0: print("Step "+str(step)+', time = '+str(float(dt) * step ))
                
                # Set time in analytical solution
                phi_expr.t = (step) * float(dt)
                
                if Uh_list[i] != 0.:
                    shift_t, rlocate_t = lp.step(uadvect, dt=float(dt), stepnum = step -1)
                
                # Perform projection
                pde_projection.assemble(lp, 'Scalar', [bc_a])
                pde_projection.solve_global(psibarh)
                pde_projection.backsubstitute(psibarh, phih_star)
                
                # Do diffusion step 
                if float(kappa) != 0.:
                    diff_sc.assemble_rhs([bc_d])
                    diff_sc.solve_global(phibarh)
                    diff_sc.backsubstitute(phibarh, phih)
                    
                    assign(dphi00, dphi0)
                    assign(dphi0, project(phih-phih_star,W))
                    assign(phibarh0, phibarh)
                    lp.mesh_to_particles_flip(phih, phih_star, 
                        'Scalar', 'deltaScalar', step-1, float(thetaP_list[i]))                    
                else:
                    assign(phih, phih_star)
                
                # Update psih0
                assign(psih0, phih_star)   
                
                if thetaDiff_list[i] != 1.0:
                    # Then also update phih0
                    assign(phih0, phih)

                if consistency_term[i]:
                    if step == 2: theta_L.assign(0.5)  
                
                l2_error   = sqrt(assemble(dot(phih - phi_expr, phih - phi_expr)*dx) )
                if comm.Get_rank() == 0: print("L2 Error "+str(l2_error))
            
            # Compute area at t = 1
            area_n = assemble(phih*dx)
            
            # Compute error
            Vhigh       = FunctionSpace(mesh,"DG",8)
            phi_ex_h    = interpolate(phi_expr, Vhigh)
            l2_error    = sqrt(assemble(dot(phih - phi_ex_h, phih - phi_ex_h)*dx) )
            
            # Save final solution
            outfile = File(outdir+'phih_nx'+str(nx)+'.pvd')
            outfile_error = File(outdir+'dphih_nx'+str(nx)+'.pvd')
            
            # Replace with 
            outfile       = File(outdir+'phih_nx'+str(nx)+'_dt_'+str(float(dt))+'.pvd')
            outfile_error = File(outdir+'dphih_nx'+str(nx)+'_dt_'+str(float(dt))+'.pvd')
            
            outfile << phih
            outfile_error << project(phih - phi_ex_h, W)
            
            # Print and Store error and area error
            num_cells_p = comm.gather(mesh.num_cells(), root = 0) 
            mesh_hmin   = comm.gather(mesh.hmin(), root = 0)
            mesh_hmax   = comm.gather(mesh.hmax(), root = 0)
            if comm.Get_rank() == 0:
                num_cells_t = sum(num_cells_p)
                num_particles = len(x)
                mesh_hmin_t = min(mesh_hmin)
                mesh_hmax_t = max(mesh_hmax)
                
                area_error = area_n-area_0
                print("Area Error, must be machine precision for pure advection problems"+str(area_error))
                
                print("Num cells "+str(num_cells_t))
                print("Num particles "+str(num_particles))
                print("L2 Error "+str(l2_error))

                with open(error_table, "a") as write_file:
                    write_file.write("%10.5g & %6d & %10.5g & %10.5g & %8d & %8.2e & %10.3g \\\\ \n" % 
                                                    (float(dt), int(num_cells_t), float(mesh_hmin_t), float(mesh_hmax_t),  int(num_particles),
                                                    float(l2_error), float(area_error)))       

