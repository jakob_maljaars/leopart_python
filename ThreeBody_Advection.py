"""
    Script for running the rigid body rotation of a Gaussian hump,
    a pointy cone and a slotted disk on a circular domain.
"""

from dolfin import *
from mshr import *
from mpi4py import MPI as pyMPI
import time as tm
import numpy as np 
import os
##
from LEoPart import *

comm = pyMPI.COMM_WORLD

### Input block
outdir_base = './Results/ThreeBody_Advection_TEST/'

# Geometric properties
nx_list = [64, 128]
x0,y0   = 0., 0.       # Center of domain
r       = .5           # Radius of domain

# Polynomial orders
k_list  = [1]
l_list  = [0]
kbar_list = k_list

# Particle resolution
pres= [200] #[800, 1600]
# Avoid particle placement outside disk-shaped domain
eps = [1e-3, 2.5e-4] 

# Time stepping related
dt_list         = [Constant(0.01), Constant(0.005)]
Tend            = 2.
theta_list      = [Constant(1.0)] 
beta            = Constant(1e-6)
advection_scheme= 'RK3'

# Rigid body rotation
uh              = Expression(('-pi*x[1]','pi*x[0]'),degree=3) 

# Standard deviation of Gaussian hump
sigma     = Constant(0.1)

# Update particle property by interpolation from mesh? True/False
# Switch to True will result in some smoothing of the discontinuities
update_particle_property = [False, False]
### End input block 

# Define Initial Condition classes
class Cone(Expression):
    def __init__(self,radius, center, height, **kwargs):
        self.r      = radius
        self.height = height
        self.center = center
        
    def eval(self, value, x):        
        xc = self.center[0]
        yc = self.center[1]
        
        if  ((x[0] - xc)**2 + (x[1] - yc)**2 <=self.r**2):
            value[0] = self.height * (1. - 1./self.r *np.sqrt((x[0] - xc)**2 + (x[1] - yc)**2))
        else:
            value[0] = 0.
    def value_shape(self):
        return ()

class Zalesack(Expression):
    def __init__(self,radius, center, width, depth, **kwargs):
        self.r      = radius
        self.width  = width
        self.depth  = depth
        self.center = center
        
    def eval(self, value, x):
        xc = self.center[0]
        yc = self.center[1]
        
        if  ((x[0] - xc)**2 + (x[1] - yc)**2 <=self.r**2) \
            and not ( (xc - self.width) <= x[0] <=  (xc + self.width)  and  x[1] >= yc + self.depth):
            value[0] = 1.
        else:
            value[0] = 0.
            
    def value_shape(self):
        return ()

class Gaussian(Expression):
    def __init__(self,center, sigma, height = 1.0, **kwargs):
        self.center = center
        self.sigma  = sigma
        self.height = height
        
    def eval(self, value, x):
        xc = self.center[0]
        yc = self.center[1]
        value[0] = self.height * np.exp(-(pow(x[0] - xc,2)+pow(x[1]-yc,2))/(2*pow(self.sigma,2)))
    
    def value_shape(self):
        return () 

# Initial condition
psi_cone        = Cone(radius = 0.15, center = [-0.3, 0.], height = 1., degree = 3)  
psi_zalesack    = Zalesack(radius = 0.15, center = [0., -0.3], width = 0.05, depth = 0., degree = 3) 
psi_gaussian    = Gaussian(center = [0.15, 0.15], sigma = float(sigma), degree = 3) 

# Helper functions
def assign_particle_values(x, psi_exact):
    if comm.Get_rank() == 0:
        s=np.asarray([psi_exact(x[i,:]) for i in range(len(x))], dtype = np.float_)
    else:
        s = None
    return s

class OuterBound(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary  

def facet_integral(integrand):
    return integrand('-')*dS + integrand('+')*dS + integrand*ds

for i, (k,l,kbar) in enumerate(zip(k_list, l_list, kbar_list)):
    # Set information for output
    outdir      = outdir_base+'Case_k'+str(k)+'l'+str(l)+'kbar' \
                    +str(kbar)+'/'
    error_table = outdir+'error_table'
    
    theta       = theta_list[i]   
    for j,nx in enumerate(nx_list):
        outfile = File(outdir+'phih_nx'+str(nx)+'.pvd')
        if comm.Get_rank() == 0:
            print("Starting computation with grid resolution "+str(nx))
        
        dt        = dt_list[j]
        num_steps = np.rint(Tend/float(dt))
            
        # Generate mesh
        domain = Circle(Point(x0,y0),r,nx*4)
        mesh = generate_mesh(domain,nx)
        mesh.init()
        n = FacetNormal(mesh)
                       
        # Create function spaces
        W      = FunctionSpace(mesh, 'DG', k)
        T      = FunctionSpace(mesh, 'DG', l)
        Wbar   = FunctionSpace(mesh, 'DGT',kbar)
               
        psih, psih0       = Function(W), Function(W)
        lambh             = Function(T)
        psibarh           = Function(Wbar)
        
        # Make initial condition
        psi_c, psi_z, psi_g = Function(W), Function(W), Function(W)
        psi_c.assign(psi_cone)
        psi_z.assign(psi_zalesack)
        psi_g.assign(psi_gaussian)       
        psih0.assign(psi_c + psi_z + psi_g)
        psi_expr = project(psih0, W)
        outfile << psih0
        
        # Initialize PDE constrained map. Please note: \theta_L consistency term NOT required!
        FuncSpace_adv  = {'FuncSpace_local': W, 'FuncSpace_lambda': T , 'FuncSpace_bar': Wbar}
        forms_adv      = FormsPDEMap(mesh, FuncSpace_adv).forms_theta_linear(psih0, uh, dt, theta)
        pde_projection = PDEMap(mesh, forms_adv['N_a'], forms_adv['G_a'], forms_adv['L_a'],
                                                                          forms_adv['H_a'], 
                                                                          forms_adv['B_a'],
                                      forms_adv['Q_a'], forms_adv['R_a'], forms_adv['S_a'])
               
        bc = DirichletBC(Wbar, Constant(0.), OuterBound())
        
        # Define particles
        x = RandomCircle(Point(x0, y0), r-eps[j], seed = 20).generate([pres[j], pres[j]])
        x = comm.bcast(x, root=0) #, comm.bcast(s, root=0)
        
        num_cells_p = comm.gather(mesh.num_cells(), root = 0) 
        mesh_hmin   = comm.gather(mesh.hmin(), root = 0)
        mesh_hmax   = comm.gather(mesh.hmax(), root = 0)
        if comm.Get_rank() == 0:
            print('Num cells total '+str(sum(num_cells_p)))
            print('Num particles '+str(len(x)))
        
        
        ## Initialize particles and advective flow field
        V = VectorFunctionSpace(mesh, 'CG', 5)                 
        uadvect = interpolate(uh,V)
        lp = LagrangianParticles(V, advection_scheme)
        lp.add_particles(x, properties_d = {'Scalar': [None]*len(x)})
        lp.mesh_to_particles(psih0,'Scalar')   # Set particle values
        
        # Machinery needed to compute local mass conservation error
        MassConservationSwitch = FunctionSpace(mesh,'DG', 0)
        mcons = Function(MassConservationSwitch)
        mloc3 = mcons * (psih - psih0)/dt* dx + facet_integral( mcons * dot(uh,n) * psibarh )      
        mloc3 = Form(mloc3)       
        
        area_0 = assemble(psih0*dx)
        step = 0
        while step < num_steps:
            step += 1
            if comm.Get_rank() == 0: print("Step "+str(step))     
            shift_t, rlocate_t = lp.step(uadvect, dt=float(dt))
            
            # Perform projection
            pde_projection.assemble(lp, 'Scalar', [bc])
            pde_projection.solve_global(psibarh)
            pde_projection.backsubstitute(psibarh, psih)
                        
            if update_particle_property[i]:
                lp.mesh_to_particles(psih,'Scalar')
                        
            if int(2*step  - num_steps ) == 0:
                if comm.Get_rank() == 0:
                    print('Computing area at half rotation')
                area_half = assemble(psih*dx)
                                
                # If run in serial, compute local errors.
                if comm.Get_size() == 1:
                    local_mass_error_half = 0.
                    for cell in cells(mesh):
                        local_vec = np.zeros(len(mcons.vector().array()))
                        local_vec[cell.index()] = 1.
                        mcons.vector().set_local(local_vec)
                    
                        local_mass_error_half += assemble( mloc3) **2   
                    local_mass_error_half = sqrt(local_mass_error_half)
                    print('Local mass conservation error '+str(local_mass_error_half))
                else:
                    print('Parallel run, computing local mass conservation not yet implemented')
            
            if comm.Get_size() == 1:        
                if int(step  - num_steps ) == 0:
                    # Compute local mass error
                    local_mass_error_full = 0.
                    for cell in cells(mesh):
                        local_vec = np.zeros(len(mcons.vector().array()))
                        local_vec[cell.index()] = 1.
                        mcons.vector().set_local(local_vec)
                        local_mass_error_full += assemble( mloc3) **2    
                    local_mass_error_full = sqrt(local_mass_error_full)
                    print('Local mass conservation error '+str(local_mass_error_full))
            else:
                print('Parallel run, computing local mass conservation not yet implemented')
            
            # Update old solution
            assign(psih0, psih)
            
            # And store
            if step % 10 is 0 or step is 1:
                outfile << psih0
            
                   
        # Compute area end
        area_n = assemble(psih*dx)
        
        # Compute error compared to initial condition
        l2_error   = sqrt(assemble(dot(psih - psi_expr, psih - psi_expr)*dx) )
                       
        # Print and Store error and area error
        num_cells_p = comm.gather(mesh.num_cells(), root = 0) 
        mesh_hmin   = comm.gather(mesh.hmin(), root = 0)
        mesh_hmax   = comm.gather(mesh.hmax(), root = 0)
                
        if comm.Get_rank() == 0:
            num_cells_t   = sum(num_cells_p)
            num_particles = len(x)
            mesh_hmin_t   = min(mesh_hmin)
            mesh_hmax_t   = max(mesh_hmax)
            
            print("Num cells "+str(num_cells_t))
            print("Num particles "+str(num_particles))
            print("L2 Error "+str(l2_error))
            area_error_half = np.float64((area_half-area_0)/area_0)
            area_error_end  = np.float64((area_n-area_0)/area_0)
            print("Area Error half "+str(area_error_half))
            print("Area Error end "+str(area_error_end))
            with open(error_table, "a") as write_file:
                if comm.Get_size() == 1: 
                    write_file.write("%10.5g & %6d & %10.5g & %10.5g & %8d & %8.2e  & %10.3g & %10.3g & %10.3g & %10.3g \\\\ \n" % 
                                    (float(dt), int(num_cells_t), float(mesh_hmin_t), float(mesh_hmax_t),  int(num_particles),
                                    float(l2_error), np.float64(area_error_half), np.float64(area_error_end),
                                    np.float64(local_mass_error_half), np.float64(local_mass_error_full)))
                else: 
                    write_file.write("%10.5g & %6d & %10.5g & %10.5g & %8d & %8.2e  & %10.3g & %10.3g \\\\ \n" % 
                                    (float(dt), int(num_cells_t), float(mesh_hmin_t), float(mesh_hmax_t),  int(num_particles),
                                    float(l2_error), np.float64(area_error_half), np.float64(area_error_end)))
                                    

