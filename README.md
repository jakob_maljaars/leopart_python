## LEoPart
Code in support of the paper Maljaars et al., Conservative, high-order particle-mesh scheme with applications to
advection-dominated flows, [CMAME (2019)](https://doi.org/10.1016/j.cma.2019.01.028), preprint available at [arXiv](https://arxiv.org/abs/1806.09916)

---

## Dependencies
LEoPart requires FEniCS 2016.1.0 but should work for higher versions also.
(www.fenicsproject.org)

LEoPart includes parts of the LagrangianParticle
functionality from the fenicstools library. See https://github.com/mikaem/fenicstools

---

## Newer version
**A better, cleaner and faster replacement of this purely Python based library,
is hosted at**
<https://bitbucket.org/jakob_maljaars/leopart>

---

## Installation and executing the code
1. Clone the repo via

    git clone https://jakob_maljaars@bitbucket.org/jakob_maljaars/leopart.git

2. IF you have FEniCS (>2016.1.0) installed locally, you are all set to execute the code (GoTo 4).

3. IF you want to use fenics from within a docker container run

    [sudo] docker run -ti -v $(pwd):/home/fenics/shared quay.io/fenicsproject/stable:[YOUR_PREFERED_VERSION]

    from within the directory where the LEoPart clone is located. The LEoPart functionality and benchmarks
    can be found in /home/fenics/shared and you are all set to execute the code (GoTO 4).

4. The code runs in parallel and can be executed as, e.g.,

    mpirun -np 4 python Gaussian_AdvectionDiffusion.py

    for running the advection-diffusion benchmark.

---

## Reproducing results
Numerical results from Section 5 are reproduced via:

* The advection-diffusion of a Gaussian, Section 5.1: run Gaussian_AdvectionDiffusion.py
* The three-body rotation test, Section 5.2: run ThreeBody_Advection.py. To compute local mass conservation error, run code in serial (sorry about that).
* The skew advection test, Section 5.3, run SkewAdvection.py
* The convergence analysis for the Poiseuille benchmark, Section 5.4.1, run Poiseuille.py
* The particle resolution test, Section 5.4.2, run Poiseuille_PresTest.py
* The convergence analysis for the Taylor-Green test, Section 5.5: run TaylorGreen.py

Relevant output is stored in .txt files.

## Contact
Any questions or suggestions? Feel free to contact the authors:
j.m.maljaars _at_ tudelft.nl / jakobmaljaars _at_ gmail.com

## License
Copyright (C) 2018 Maljaars et al.

This software can be redistributed and/or modified under the terms of the GNU Lesser General Public License as published by the Free Software Foundation (<http://www.gnu.org/licenses/>).

The software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.



