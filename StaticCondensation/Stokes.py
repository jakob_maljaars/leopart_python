__author__ = "Jakob Maljaars <j.m.maljaars@tudelft.nl>"
__date__ = "2018-02-06"
__copyright__ = "Copyright (C) 2018 " + __author__
__license__ = "GNU Lesser GPL version 3 or any later version"

from dolfin import *
import numpy as np

__all__ = ['FormsStokes', 'Stokes_StaticCondensation']

class FormsStokes:
    """
    Initializes the forms for the unsteady Stokes problem
    following Labeur and Wells (2012) and 
    Rhebergen and Wells (2016,2017).
    
    It defines the forms in correspondence with following 
    algebraic form:
    
    |  A   B   C   D  | | Uh    |    
    |  B^T F   0   H  | | Ph    |    |Q| 
    |                 | |       | =  | |
    |  C^T 0   K   L  | | Uhbar |    |S| 
    |  D^T H^T L^T P  | | Phbar | 
    
    With part above blank line indicating the contributions
    from local momentum- and local mass conservation statement
    respectively. Part below blank line indicates the contribution
    from global momentum and global mass conservation statement.    
    """
    
    def __init__(self, mesh,FuncSpaces_L, FuncSpaces_G, k, beta_stab = Constant(0.)):
        self.mixedL     = FuncSpaces_L
        self.mixedG     = FuncSpaces_G
        self.n          = FacetNormal(mesh)
        self.beta_stab  = beta_stab
        self.alpha      = Constant(6*k*k)
        self.he         = CellSize(mesh)
    
    def forms(self,ustar,dt,nu,f):
        '''
        Forms for the HDG method of 
        Labeur and Wells (2012) and 
        Rhebergen and Wells (2016/2017).
        
        Note that we can easiliy change between 
        the two formulations since pressure stabilization 
        term required for Labeur and Wells (2012) formulation is 
        supported. 
        '''
        
        w, q = TestFunctions(self.mixedL)
        u, p = TrialFunctions(self.mixedL)
        wbar, qbar = TestFunctions(self.mixedG)
        ubar, pbar = TrialFunctions(self.mixedG)
        
        n = self.n; he = self.he;
        alpha = self.alpha; beta_stab = self.beta_stab
        facet_integral = self.facet_integral
        
        pI = p*Identity(self.mixedL.sub(1).ufl_cell().topological_dimension())
        pbI= pbar*Identity(self.mixedL.sub(1).ufl_cell().topological_dimension())        
        
        AB = dot(u,w)/dt * dx \
            + inner( 2*nu*sym(grad(u)),grad(w) )*dx \
            + facet_integral( dot(-2*nu*sym(grad(u))*n + (2*nu*alpha/he)*u,w) ) \
            + facet_integral( dot(-2*nu*u,sym(grad(w))*n) ) \
            - inner(pI,grad(w))*dx
        BtF= dot(u,grad(q))*dx - facet_integral(dot(u,n) * q)- facet_integral(beta_stab*he/(nu+1)*dot(p,q))
        A_S  = AB + BtF

        # Upper right block G
        CD= facet_integral(-alpha/he*2*nu*inner( ubar,w ) ) \
            + facet_integral( 2*nu*inner(ubar, sym(grad(w))*n) ) \
            + facet_integral(dot(pbI*n,w))   
        H = facet_integral(beta_stab*he/(nu+1)*dot(pbar,q))
        G_S = CD + H
        
        # Lower right block B
        KL  = facet_integral( alpha/he * 2*nu*dot(ubar,wbar)) - facet_integral( dot(pbar*n,wbar) )
        LtP = -dot(ubar,n)*qbar * ds - facet_integral( beta_stab*he/(nu+1) * pbar * qbar ) 
        B_S = KL + LtP
        
        #Righthandside
        Q_S = dot(f,w)*dx + dot(ustar,w)/dt * dx  
        S_S = facet_integral( dot( Constant((0,0)), wbar))
        return self.__get_form_dict(A_S, G_S, B_S, Q_S, S_S)
    
    def facet_integral(self, integrand):
        return integrand('-')*dS + integrand('+')*dS + integrand*ds
    
    def __get_form_dict(self, A_S, G_S, B_S, Q_S, S_S):   
        # Turn into forms
        A_S  = Form(A_S); G_S = Form(G_S); B_S = Form(B_S)
        Q_S  = Form(Q_S); S_S = Form(S_S)        
        return {'A_S': A_S, 'G_S': G_S, 'B_S': B_S, 'Q_S': Q_S, 'S_S': S_S}
    
class Stokes_StaticCondensation:
    """
    Class for solving the unsteady Stokes equations 
    using a static condensation procedure, corresponding to 
    algebraic form:
    
    |  N   G   | | Uh    |   | Q |
    |          | | Ph    |   |   |
    |          | |       | = |   |
    |          | | Uhbar |   |   |
    |  G^T B   | | Phbar |   | S |
    
    """
    def __init__(self,mesh,N,G,B,Q,S):
        self._assert_input(N, G, B, Q, S)
        
        # Set info
        self.N = N; self.G = G; self.B = B; 
        self.Q = Q; self.S = S
        
        # Get cell-dof lists (of local and global mixed problem!)
        self.c2d_local = self._dof_properties(mesh, N.function_space(0))
        self.c2d_bar   = self._dof_properties(mesh, B.function_space(0))
        self.mesh = mesh
    
    def assemble(self, bc_list = None):
        A_g = assemble(self.B)
        f_g = assemble(self.S)
        A_g.zero()
        f_g.zero()
        
        invN_list = []; G_list = []; Q_list = []
        
        for i,cell in enumerate(cells(self.mesh)):
            N_e    = assemble_local(self.N,cell)
            G_e    = assemble_local(self.G,cell)
            B_e    = assemble_local(self.B, cell)
            Q_e    = assemble_local(self.Q, cell)
            S_e    = assemble_local(self.S, cell)
            
            invN   = np.linalg.inv(N_e)
            # Store for later use in backsubstitute
            invN_list.append(invN); G_list.append(G_e); Q_list.append(Q_e)
            
            LHS_e = G_e.T.dot(invN).dot(G_e) - B_e
            RHS_e = G_e.T.dot(invN).dot(Q_e) - S_e
            
            # Add to matrix
            idof_qb = np.asarray(self.c2d_bar[i], dtype = np.intc)
            A_g.add_local(LHS_e,idof_qb,idof_qb)
            f_g.add_local(RHS_e,idof_qb)
            
        # Finalize assembling
        A_g.apply("add")
        f_g.apply("add")
        
        # Set attributes
        self.A = A_g; self.b = f_g
        self.invN_list = invN_list; self.G_list = G_list
        self.Q_list    = Q_list
        
        if bc_list is not None:
            self._apply_bc(bc_list)  
            
    def assemble_rhs(self, bc_list = None):
        f_g = assemble(self.S)
        f_g.zero()
        
        Q_list = []
        for i, cell in enumerate(cells(self.mesh)):
            Q_e    = assemble_local(self.Q, cell)
            S_e    = assemble_local(self.S, cell)
            G_T_e  = self.G_list[i].T
            Q_list.append(Q_e)

            RHS_e = G_T_e.dot(self.invN_list[i]).dot(Q_e) - S_e
            
            idof_qb = np.asarray(self.c2d_bar[i], dtype = np.intc)
            f_g.add_local(RHS_e,idof_qb)
        
        f_g.apply("add")
        if bc_list is not None:
            for bc in bc_list:
                bc.apply(f_g)
        self.b = f_g
        self.Q_list = Q_list
    
    def solve_global(self, function_bar):
        assert function_bar.function_space() == self.B.function_space(0), \
            'Wrong function space!'
        solve(self.A, function_bar.vector(),self.b)
    
    def backsubstitute(self, function_bar, function_local):
        assert function_bar.function_space() == self.B.function_space(0), \
            'Wrong global function space!'
        assert function_local.function_space() == self.N.function_space(0), \
            'Wrong local function space!'
        
        for i,cell in enumerate(cells(self.mesh)):
            idof_q1 = np.asarray(self.c2d_local[i],dtype=np.intc)
            idof_qb = np.asarray(self.c2d_bar[i],dtype=np.intc)
            
            bar_vec = np.zeros(len(idof_qb), dtype=np.float)
            function_bar.vector().get_local(bar_vec, idof_qb )
            local_vec = self.invN_list[i].dot(self.Q_list[i] - self.G_list[i].dot( bar_vec ) )
            function_local.vector().set_local( local_vec, idof_q1 )     
    
    def _dof_properties(self, mesh, function_space):
        """
        Make array of dof numbers per cell 
        (array has dimension (fspace_dim*ndofs) x n_cells)
        """
        dofmap  =       function_space.dofmap()
        cell_to_dof = [ [dof for dof in dofmap.cell_dofs(cell)] \
                        for cell in range(mesh.num_cells())]
        np.asarray(cell_to_dof)
        return cell_to_dof
    
    def _assert_input(self, N, G, B, Q, S):
        assert type(N) is dolfin.fem.form.Form \
               and type(G) is dolfin.fem.form.Form \
               and type(B) is dolfin.fem.form.Form \
               and type(Q) is dolfin.fem.form.Form \
               and type(S) is dolfin.fem.form.Form, \
               'Wrong input type, must be dolfin.fem.form.Form' 
        assert N.rank() is 2 and G.rank() is 2 \
            and B.rank() is 2, 'LHS contribution is of wrong rank'
        assert Q.rank() is 1 and S.rank() is 1, \
            'LHS contribution contains wrong rank'
    
    def _apply_bc(self, bc_list):
        for bc in bc_list:
            bc.apply(self.A, self.b)