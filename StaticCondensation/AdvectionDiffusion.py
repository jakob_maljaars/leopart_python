__author__ = "Jakob Maljaars <j.m.maljaars@tudelft.nl>"
__date__ = "2018-02-06"
__copyright__ = "Copyright (C) 2018 " + __author__
__license__ = "GNU Lesser GPL version 3 or any later version"

from dolfin import *
import numpy as np

__all__ = ['FormsDiffusion', 'Diffusion_StaticCondensation']

class FormsDiffusion:
    """
    Initializes the forms for the diffusion problem
    following Labeur and Wells (2007)
    """
    
    def __init__(self, mesh, FuncSpace_dict, ):
        self.W     = FuncSpace_dict['FuncSpace_local'] 
        self.Wbar  = FuncSpace_dict['FuncSpace_bar']
        
        self.n = FacetNormal(mesh)
        self.he= CellSize(mesh)
        
    def forms_theta(self, phih_star, phih0, phibarh0d, dt, 
                    kappa, alpha, f, theta_d):
        phi   ,  w   = TrialFunction(self.W), TestFunction(self.W)
        phibar, wbar = TrialFunction(self.Wbar), TestFunction(self.Wbar)
        
        facet_integral = self.facet_integral
        he = self.he; n = self.n 
        beta_d   =  -alpha*kappa / he 
        
        # LHS contributions
        A_d   = 1./dt * phi * w * dx \
                + theta_d * dot( kappa*grad(phi), grad(w) ) * dx \
                - theta_d * facet_integral(   kappa * dot(grad(phi),n) * w    \
                                            + kappa * dot(phi * n, grad(w)) ) \
                - theta_d * facet_integral(beta_d * phi * w) 
        G_d   = theta_d * facet_integral(beta_d * phibar * w) \
                + theta_d * facet_integral(kappa * dot(phibar*n, grad(w)))
        G_T_d = -theta_d * facet_integral(beta_d * phi * wbar) \
                - theta_d * facet_integral(kappa * dot(wbar*n, grad(phi)))
        B_d   = theta_d * facet_integral(beta_d * phibar * wbar)

        # RHS contributions
        F_d = 1./dt * phih_star * w * dx \
                - (1-theta_d) * dot( kappa*grad(phih0), grad(w) ) * dx \
                + (1-theta_d) * facet_integral(   kappa * dot( grad(phih0),n ) * w  \
                                                + kappa * dot(phih0 * n, grad(w)) ) \
                + (1-theta_d) * facet_integral(beta_d * phih0 * w) \
                - (1-theta_d) * facet_integral(beta_d * phibarh0d * w) \
                - (1-theta_d) * facet_integral(kappa * dot(phibarh0d*n, grad(w)))\
                + f*w*dx
        H_d = facet_integral( Constant(0) * wbar) \
                + (1-theta_d) * facet_integral(beta_d * phih0 * wbar) \
                + (1-theta_d) * facet_integral(kappa * dot(wbar*n, grad(phih0))) \
                - (1-theta_d) * facet_integral(beta_d * phibarh0d * wbar)
        return self.__get_form_dict(A_d, G_d, G_T_d, B_d, F_d, H_d)
            
    def facet_integral(self, integrand):
        return integrand('-')*dS + integrand('+')*dS + integrand*ds
    
    def __get_form_dict(self, A_d, G_d, G_T_d, B_d, F_d, H_d):
        # Turn into forms
        A_d = Form(A_d); G_d = Form(G_d); G_T_d = Form(G_T_d); 
        B_d = Form(B_d); F_d = Form(F_d); H_d = Form(H_d)
       
        # Return dict of forms
        return {'A_d': A_d, 'G_d': G_d, 'G_T_d': G_T_d, 'B_d': B_d,
                'F_d': F_d, 'H_d': H_d}    

class Diffusion_StaticCondensation:
    """
    Class for solving the diffusion equation of Labeur 
    and Wells (2007), using a static condensation 
    strategy.
    Expect the forms to correspond to the algebraic form:
    
    |  N   G | | phi     |    |  W  |  
    |        | |         | =  |     |
    |  G^T B | | phi_bar |    |  S  |

    """
    
    def __init__(self, mesh, N, G, G_T, B, W, S):
        self._assert_input(N, G, G_T, B, W, S)
        
        # Set info
        self.N = N; self.G = G; self.G_T = G_T; self.B = B; 
        self.W = W; self.S = S
        
        # Get cell-dof lists
        self.c2d_local = self._dof_properties(mesh, N.function_space(0))
        self.c2d_bar   = self._dof_properties(mesh, B.function_space(0))
        self.mesh = mesh
    
    def update_forms(self, N, G, G_T, B, W, S):
        self._assert_input(N, G, G_T, B, W, S)
        
        # Set info
        self.N = N; self.G = G; self.G_T = G_T; self.B = B; 
        self.W = W; self.S = S
    
    def assemble(self, bc_list = None):
        A_g = assemble(self.B)
        f_g = assemble(self.S)
        A_g.zero()
        f_g.zero()
        
        invN_list = []; G_list = []; Q_list = []
        
        for i, cell in enumerate(cells(self.mesh)):
            N_e    = assemble_local(self.N,cell)
            G_e    = assemble_local(self.G,cell)
            G_T_e  = assemble_local(self.G_T,cell)
            B_e    = assemble_local(self.B, cell)
            Q_e    = assemble_local(self.W, cell)
            S_e    = assemble_local(self.S, cell)
            
            invN   = np.linalg.inv(N_e)
            # Store for later use in backsubstitute
            invN_list.append(invN); G_list.append(G_e); Q_list.append(Q_e)
            
            LHS_e = G_T_e.dot(invN).dot(G_e) - B_e
            RHS_e = G_T_e.dot(invN).dot(Q_e) - S_e
            
            # Assemble into global matrix
            idof_qb = np.asarray(self.c2d_bar[i], dtype = np.intc)
            A_g.add_local(LHS_e,idof_qb,idof_qb)
            f_g.add_local(RHS_e,idof_qb)
        
        # Finalize assembling
        A_g.apply("add")
        f_g.apply("add")
        
        # Set attributes
        self.A = A_g; self.b = f_g
        self.invN_list = invN_list; self.G_list = G_list
        self.Q_list    = Q_list
        
        if bc_list is not None:
            self._apply_bc(bc_list)  
    
    def assemble_rhs(self, bc_list = None):
        f_g = assemble(self.S)
        f_g.zero()
        
        Q_list = []
        for i, cell in enumerate(cells(self.mesh)):
            G_T_e  = assemble_local(self.G_T,cell)
            Q_e    = assemble_local(self.W, cell)
            S_e    = assemble_local(self.S, cell)
            
            Q_list.append(Q_e)
            RHS_e = G_T_e.dot(self.invN_list[i]).dot(Q_e) - S_e
            
            idof_qb = np.asarray(self.c2d_bar[i], dtype = np.intc)
            f_g.add_local(RHS_e,idof_qb)
        
        f_g.apply("add")
        if bc_list is not None:
            for bc in bc_list:
                bc.apply(f_g)
        self.b = f_g
        self.Q_list = Q_list
    
    def solve_global(self, function_bar):
        assert function_bar.function_space() == self.B.function_space(0), \
            'Wrong function space!'
        solve(self.A, function_bar.vector(),self.b)
    
    def backsubstitute(self, function_bar, function_local):
        assert function_bar.function_space() == self.B.function_space(0), \
            'Wrong global function space!'
        assert function_local.function_space() == self.N.function_space(0), \
            'Wrong local function space!'
        
        for i,cell in enumerate(cells(self.mesh)):
            idof_q1 = np.asarray(self.c2d_local[i],dtype=np.intc)
            idof_qb = np.asarray(self.c2d_bar[i],dtype=np.intc)
            
            bar_vec = np.zeros(len(idof_qb), dtype=np.float)
            function_bar.vector().get_local(bar_vec, idof_qb )
            local_vec = self.invN_list[i].dot(self.Q_list[i] - self.G_list[i].dot( bar_vec ) )
            function_local.vector().set_local( local_vec, idof_q1 ) 
            
    def _dof_properties(self, mesh, function_space):
        """
        Make array of dof numbers per cell 
        (array has dimension (fspace_dim*ndofs) x n_cells)
        """
        dofmap  =       function_space.dofmap()
        cell_to_dof = [ [dof for dof in dofmap.cell_dofs(cell)] \
                        for cell in range(mesh.num_cells())]
        np.asarray(cell_to_dof)
        return cell_to_dof
    
    def _assert_input(self, N, G, G_T, B, W, S):
        assert type(N) is dolfin.fem.form.Form \
               and type(G) is dolfin.fem.form.Form \
               and type(G_T) is dolfin.fem.form.Form \
               and type(B) is dolfin.fem.form.Form \
               and type(W) is dolfin.fem.form.Form \
               and type(S) is dolfin.fem.form.Form, \
               'Wrong input type, must be dolfin.fem.form.Form' 
        assert N.rank() is 2 and G.rank() is 2 and G_T.rank() is 2 \
            and B.rank() is 2, 'LHS contribution contains wrong rank'
        assert W.rank() is 1 and S.rank() is 1, \
            'LHS contribution contains wrong rank'
    
    def _apply_bc(self, bc_list):
        for bc in bc_list:
            bc.apply(self.A, self.b)