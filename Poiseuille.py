"""
    Script for running the convergence test for Poiseuille flow
"""

from dolfin import *
from mpi4py import MPI as pyMPI
import numpy as np
import os

from LEoPart import *
from StaticCondensation import FormsStokes, Stokes_StaticCondensation

##
from poiseuille_analytical import AnalyticalSolution

comm = pyMPI.COMM_WORLD

### Input block
outdir_base = './Results/Poiseuille_Convergence/'
xmin = 0.; xmax = 1.
ymin = -.25; ymax = .25

factor_list = [1, 2, 4, 8] 
nxbase   = 8
nybase   = 4
pres_base= 60
nu       = Constant(1E-3)
dt_base  = 20e-2
klist  = [1, 2, 2]; 
llist = [0]*len(klist); kbarlist = klist

# Consistency 
consistency_term = [True, True, False]

Tend       = 125.0
beta       = Constant(1e-6)
theta_map  = Constant(1.0)
advection_scheme = 'AB2'

# Body force
fx      = 0.0128
fy      = 0.
f = Constant((fx,fy))

# Overload SubDomain class to define PeriodicBoundary
boundary_dict = dict(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)

class PeriodicBoundary(SubDomain):
    # Left boundary is "target domain" G
    def __init__(self,bdict):
        SubDomain.__init__(self)
        self.xmin, self.xmax = bdict['xmin'], bdict['xmax']
        self.ymin, self.ymax = bdict['ymin'], bdict['ymax']
    
    def inside(self, x, on_boundary):
        return bool(near(x[0],self.xmin) and on_boundary)
    
    def map(self, x, y):
        if near(x[0], self.xmax):
            y[0] = x[0] - (self.xmax - self.xmin)
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1]
            
class Lower(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and x[1] < -0.25 + DOLFIN_EPS

class Upper(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and x[1] >  0.25 - DOLFIN_EPS

class Corner(SubDomain):
    def inside(self,x, on_boundary):
        return near(x[0], 1) and near(x[1], 0.)

for i,(k,l,kbar) in enumerate(zip(klist,llist, kbarlist)):
    k    = k
    l    = llist[i]
    kbar = kbarlist[i]
    
    outdir      = outdir_base+'nu'+str(float(nu))+'/k'+str(k)+'l'+str(l)+'kbar'+str(kbar)+'_case_'+str(i+1)+'/'
    error_table = outdir+'error_table' 
    
    try:
        os.remove(error_table)
    except OSError:
        pass

    for factor in factor_list:
        nx = nxbase  * factor ; 
        ny = nybase  * factor
        p_res = pres_base * factor
        dt = Constant(dt_base/float(factor))
        num_steps = np.rint(Tend/float(dt))
        
        U_exact = ('-U*exp(-2*nu*pow(pi,2)*t)*(cos(pi*(x[0]))*sin(pi*(x[1])))',
                ' U*exp(-2*nu*pow(pi,2)*t)*(cos(pi*(x[1]))*sin(pi*(x[0])))')
        u_exact = Expression(U_exact, degree=3, U=float(1.), nu=float(nu),t=float(0.))
        P_exact = ('-0.25*exp(-4*nu*pow(pi,2)*t)*(cos(2*pi*(x[0])) + cos(2*pi*(x[1]))-2.0)')
        p_exact = Expression(P_exact, degree = 3, nu=float(nu),t=float(0.))
        
        # Create mesh
        mesh = RectangleMesh(Point(xmin,ymin), Point(xmax,ymax), nx,ny)   

        # Define the ufl-elements 
        V_E      = VectorElement("DG", mesh.ufl_cell(), k)
        T_E    = VectorElement("DG", mesh.ufl_cell(), 0)
        Vbar_E = VectorElement("DGT", mesh.ufl_cell(), kbar)

        Q_S    = FiniteElement("DG", mesh.ufl_cell(), k-1)
        Qbar_S = FiniteElement("DGT", mesh.ufl_cell(), k)

        # Define spaces 
        # For advection:
        W    = FunctionSpace(mesh,V_E, constrained_domain = PeriodicBoundary(boundary_dict))
        T      = FunctionSpace(mesh,T_E, constrained_domain = PeriodicBoundary(boundary_dict))
        Wbar   = FunctionSpace(mesh,Vbar_E, constrained_domain = PeriodicBoundary(boundary_dict) ) 

        # For Stokes
        mixedL = FunctionSpace(mesh, MixedElement([V_E,Q_S]), constrained_domain = PeriodicBoundary(boundary_dict))
        mixedG = FunctionSpace(mesh, MixedElement([Vbar_E,Qbar_S]),constrained_domain = PeriodicBoundary(boundary_dict))
        
        # Define (solution) funcitons
        v0      = Function(W);  ustar   = Function(W); Udiv    = Function(W)
        duh0    = Function(W);  duh00   = Function(W)
        Ubar0 = Function(Wbar); vbarh   = Function(Wbar)
        
        Uh_S    = Function(mixedL); Uh0_S   = Function(mixedL)
        Uhbar_S = Function(mixedG); Uhbar0_S= Function(mixedG)
        
        # Define boundary conditions advection problem
        boundaries = FacetFunction("size_t", mesh) 
        boundaries.set_all(0)
        NeumannUpper  = Upper()
        NeumannLower  = Lower()
        NeumannUpper.mark(boundaries,99)
        NeumannLower.mark(boundaries,99)
        ds = Measure('ds', domain=mesh, subdomain_data=boundaries)
        
        # Get forms for PDE-constrained advection including add_consistency_term
        theta_L         = Constant(1.)
        FuncSpace_adv   = {'FuncSpace_local': W, 'FuncSpace_lambda': T , 'FuncSpace_bar': Wbar}
        forms_adv       = FormsPDEMap(mesh, FuncSpace_adv).forms_theta_nlinear(v0, Ubar0, dt,
                                                                               theta_map, theta_L = theta_L,
                                                                               duh0 = duh0, duh00 = duh00)
        bc1_a = DirichletBC(Wbar, Constant((0.,0.)),   Lower())
        bc2_a = DirichletBC(Wbar, Constant((0.,0.)),   Upper())
        
        # Get forms for Stokes step
        forms_stokes    = FormsStokes(mesh,mixedL,mixedG,k).forms(ustar,dt,nu,f)
        
        # Define boundary conditions Stokes step
        bc1_s = DirichletBC(mixedG.sub(0), Constant((0.,0.)),   Lower())
        bc2_s = DirichletBC(mixedG.sub(0), Constant((0.,0.)),   Upper())
        bc3_s = DirichletBC(mixedG.sub(1), Constant(0.),   Corner(), "pointwise")
        bc_S = [bc1_s, bc2_s, bc3_s]
        
        # Initialize solver classes for PDE-projection and Stokes step
        pde_projection = PDEMap(mesh, forms_adv['N_a'], forms_adv['G_a'], forms_adv['L_a'],
                                                                          forms_adv['H_a'], 
                                                                          forms_adv['B_a'],
                                      forms_adv['Q_a'], forms_adv['R_a'], forms_adv['S_a'])
        stokes_sc      = Stokes_StaticCondensation(mesh, 
                                                    forms_stokes['A_S'], forms_stokes['G_S'],
                                                                        forms_stokes['B_S'], 
                                                    forms_stokes['Q_S'], forms_stokes['S_S'])
        # Preassemble Stokes
        stokes_sc.assemble(bc_S)
        
        # Initialize Lagrangian Particles
        x = RandomRectangle(Point(xmin, ymin), Point(xmax, ymax), seed = 10).generate([p_res, p_res/2])
        x = comm.bcast(x, root=0)
        nump = len(x)
        periodic = np.array([[xmin,xmin, ymin, ymax],[xmax, xmax, ymin, ymax]])
        lp = LagrangianParticles(W, advection_scheme, periodic)
        lp.add_particles(x, properties_d = {'Velocity': [None]*nump, 'deltaVel': [None]*len(x) })
        lp.mesh_to_particles(v0,'Velocity')
            
        # Time Loop
        step = 0
        while step < num_steps:
            step += 1
            if comm.Get_rank() == 0: print("Step "+str(step)+', time = '+str(float(dt) * step )) 
            
            shift_t, rlocate_t = lp.step(Udiv, dt=float(dt), stepnum = step -1 )
            pde_projection.assemble(lp, 'Velocity',[bc1_a, bc2_a])
            pde_projection.solve_global(vbarh)
            pde_projection.backsubstitute(vbarh, ustar)
            
            # Do Stokes Step
            stokes_sc.assemble_rhs(bc_S)
            stokes_sc.solve_global(Uhbar_S)
            stokes_sc.backsubstitute(Uhbar_S, Uh_S)
            
            # Update for next time step            
            # Set advective velocity for next time step
            assign(Udiv, Uh_S.sub(0))
            
            # Set old bar function
            assign(Ubar0, Uhbar_S.sub(0))
            
            # Compute increment
            assign(duh00, duh0)
            assign(duh0, project(Uh_S.sub(0)-ustar,W))
            
            # Update particle property
            lp.mesh_to_particles_flip(Uh_S.sub(0), ustar, 
                                        'Velocity', 'deltaVel', step-1, 0.5)
            if consistency_term[i]:
                assign(v0, ustar)
                if step == 2: theta_L.assign(0.5)
            else:
                assign(v0, ustar)
                if step == 1: theta_L.assign(1.)
                
                
        # When finished, store some results
        outfile_u = File(outdir+"tg_velocity_tend_nx_"+str(nx)+".pvd")
        outfile_p = File(outdir+"tg_pressure_tend_nx_"+str(nx)+".pvd")
        outfile_u  << Uh_S.sub(0)
        outfile_p  << Uh_S.sub(1)
        
        Vhigh = VectorFunctionSpace(mesh,"DG",7)
        u_exact = AnalyticalSolution(Vhigh,fx,float(nu),ymax,Tend)()
        
        u_error = sqrt( abs( assemble(dot(Uh_S.sub(0) - u_exact, Uh_S.sub(0) - u_exact)*dx)) )
        p_error = sqrt( abs( assemble(dot(Uh_S.sub(1), Uh_S.sub(1))*dx)) )
        
        outfile_du = File(outdir+"tg_dvelocity_tend_nx_"+str(nx)+".pvd")
        outfile_dp = File(outdir+"tg_dpressure_tend_nx_"+str(nx)+".pvd")
        
        outfile_du << project(Uh_S.sub(0) - u_exact, W)
        
        num_cells_p = comm.gather(mesh.num_cells(), root = 0) 
        mesh_hmin   = comm.gather(mesh.hmin(), root = 0)
                    
        if comm.Get_rank() == 0:
            print 'L2 u_error', u_error
            print 'L2 p_error', p_error
            num_cells_t = sum(num_cells_p)
            mesh_hmin_t = min(mesh_hmin)
            num_particles = len(x)
            with open(error_table, "a") as write_file:
                write_file.write("%10.5g & %6d & %10.5g &  %8d & %8.2e  & %8.2e \\\\ \n" % 
                                                            (float(dt), int(num_cells_t), float(mesh_hmin_t),  int(num_particles),
                                                            float(u_error), float(p_error) ))

