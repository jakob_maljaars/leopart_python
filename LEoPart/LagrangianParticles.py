__authors__     = ('Mikael Mortensen <mikaem@math.uio.no>',
                    'Miroslav Kuchta <mirok@math.uio.no>',
                    'Jakob Maljaars <j.m.maljaars@tudelft.nl>')
__date__        = '2018-01-15'
__copyright__   = 'Copyright (C) 2011' + __authors__[0]
__license__     = 'GNU Lesser GPL version 3 or any later version'

'''
This module contains functionality for Lagrangian tracking of particles with
DOLFIN
'''

import dolfin as df
import numpy as np
import copy
from mpi4py import MPI as pyMPI
from collections import defaultdict
import cPickle as pickle

__all__ = ['LagrangianParticles']

# Disable printing
__DEBUG__ = False

comm = pyMPI.COMM_WORLD

# collisions tests return this value or -1 if there is no collision
__UINT32_MAX__ = np.iinfo('uint32').max

class Particle:
    __slots__ = ['position', 'properties']
    'Lagrangian particle with position and some other passive properties.'
    def __init__(self, x):
        self.position = x
        self.properties = {}

    def send(self, dest):
        'Send particle to dest.'
        comm.Send(self.position, dest=dest)
        comm.send(self.properties, dest=dest)

    def recv(self, source):
        'Receive info of a new particle sent from source.'
        comm.Recv(self.position, source=source)
        self.properties = comm.recv(source=source)

class CellWithParticles(df.Cell):
    'Dolfin cell with list of particles that it contains.'
    def __init__(self, mesh, cell_id, particle):
        # Initialize parent -- create Cell with id on mesh
        df.Cell.__init__(self, mesh, cell_id)
        # Make an empty list of particles that I carry
        self.particles = []
        self += particle

    def __add__(self, particle):
        'Add single particle to cell.'
        assert isinstance(particle, (Particle, np.ndarray))
        if isinstance(particle, Particle):
            self.particles.append(particle)
            return self
        else:
            return self.__add__(Particle(particle))

    def __len__(self):
        'Number of particles in cell.'
        return len(self.particles)


class CellParticleMap(dict):
    'Dictionary of cells with particles.'
    def __add__(self, ins):
        '''
        Add ins to map:
            ins is either (mesh, cell_id, particle) or
                          (mesh, cell_id, particle, particle_properties)
        '''
        assert isinstance(ins, tuple) and len(ins) in (3, 4)
        # If the cell_id is in map add the particle
        if ins[1] in self:
            self[ins[1]] += ins[2]
        # Otherwise create new cell
        else:
            self[ins[1]] = CellWithParticles(ins[0], ins[1], ins[2])
        # With particle_properties, update properties of the last added particle
        if len(ins) == 4:
            self[ins[1]].particles[-1].properties.update(ins[3])

        return self

    def pop(self, cell_id, i):
        'Remove i-th particle from the list of particles in cell with cell_id.'
        # Note that we don't check for cell_id being a key or cell containg
        # at least i particles.
        particle = self[cell_id].particles.pop(i)

        # If the cell is empty remove it from map
        if len(self[cell_id]) == 0:
            del self[cell_id]

        return particle

    def total_number_of_particles(self):
        'Total number of particles in all cells of the map.'
        return sum(map(len, self.itervalues()))


class LagrangianParticles:
    'Particles moved by the velocity field in V.'
    def __init__(self, V, dt_method = None, periodic_bc = None, reset_value = None):
        'Make a choice of either one of the following time stepping schemes'
        dtmethod_dict={'Euler': 1, 'RK2': 2, 'RK3': 3, 'RK4': 4, 
                       'AB2': 1, 'AB3': 1}
        
        self.__debug = __DEBUG__
        self.V = V
        self.mesh = V.mesh()
        self.mesh.init(2, 2)  # Cell-cell connectivity for neighbors of cell
        self.tree = self.mesh.bounding_box_tree()  # Tree for isection comput.
        
        # This is cumbersome... but apparently the fenics connectivity sucks
        cell_list = []
        for cell in df.cells(self.mesh):
            cell_list.append(cell)
        
        tdim = self.mesh.topology().dim()
        self.mesh.init(tdim - 1, tdim)
        self.cell_neighbors = {cell.index(): sum((filter(lambda ci: ci != cell.index(),
                                            facet.entities(tdim))
                                    for facet in df.facets(cell)), [])
                                    for cell in df.cells(self.mesh)} 
        for cell in df.cells(self.mesh):
            cell_idcs = self.cell_neighbors[cell.index()]
            self.cell_neighbors[cell.index()] = [cell_list[cell_idx] for cell_idx in cell_idcs]
            
        # Allocate some variables used to look up the velocity
        # Velocity is computed as U_i*basis_i where i is the dimension of
        # element function space, U are coefficients and basis_i are element
        # function space basis functions. For interpolation in cell it is
        # advantageous to compute the resctriction once for cell and only
        # update basis_i(x) depending on x, i.e. particle where we make
        # interpolation. This updaea mounts to computing the basis matrix
        self.dim = self.mesh.topology().dim()

        self.element = V.dolfin_element()
        self.num_tensor_entries = 1
        for i in range(self.element.value_rank()):
            self.num_tensor_entries *= self.element.value_dimension(i)
        # For VectorFunctionSpace CG1 this is 3
        self.coefficients = np.zeros(self.element.space_dimension())
        # For VectorFunctionSpace CG1 this is 3x3
        self.basis_matrix = np.zeros((self.element.space_dimension(),
                                      self.num_tensor_entries))

        # Allocate a dictionary to hold all particles
        self.particle_map = CellParticleMap()

        # Allocate some MPI stuff
        self.num_processes = comm.Get_size()
        self.myrank = comm.Get_rank()
        self.all_processes = range(self.num_processes)
        self.other_processes = range(self.num_processes)
        self.other_processes.remove(self.myrank)
        self.my_escaped_particles = np.zeros(1, dtype='I')
        self.tot_escaped_particles = np.zeros(self.num_processes, dtype='I')
        # Dummy particle for receiving/sending at [0, 0, ...]
        self.particle0 = Particle(np.zeros(self.mesh.geometry().dim()))
        if dt_method is not None:
            assert (dt_method in dtmethod_dict.keys()), 'Time integration not known!'
            self.dt_method    = dt_method
            self.num_substeps = dtmethod_dict[dt_method]
        else:
            self.dt_method    = None
        
        if periodic_bc is not None:
            self.periodic_bc = periodic_bc
        else:
            self.periodic_bc = None
        
        # A sloppy implementation of resetting particle value
        if reset_value is not None:
            self.reset_value = reset_value
        else:
            self.reset_value = None
            
    def __iter__(self):
        '''Iterate over all particles.'''
        for cwp in self.particle_map.itervalues():
            for particle in cwp.particles:
                yield particle

    def add_particles(self, list_of_particles, properties_d=None, call = None):
        '''
        Add particles and search for their home on all processors.
        Note that list_of_particles must be same on all processes. Further
        every len(properties[property]) must equal len(list_of_particles).
        '''
        
        if ( properties_d is not None or self.dt_method is not None ) \
            and call is None:
            n = len(list_of_particles)
            if properties_d is not None:
                assert all(len(sub_list) == n
                        for sub_list in properties_d.itervalues())
            
            if self.dt_method is not None:
                if self.num_substeps == 1:
                    substep_velocity = np.zeros([n,self.mesh.geometry().dim()],dtype=float)
                else:
                    substep_velocity = np.zeros([n,self.num_substeps,self.mesh.geometry().dim()],dtype=float)
                original_position= np.zeros([n,self.mesh.geometry().dim()],dtype=float)
                        
                if properties_d is not None:
                    properties_d['position_old']   = original_position
                    properties_d['velocity_sub']   = substep_velocity
                    
                    if self.dt_method is 'AB3':
                        properties_d['position_oold']   = original_position
                        properties_d['velocity_oold']   = substep_velocity      
                else:      
                     properties_d = {'position_old': original_position, 'velocity_sub': substep_velocity}
                     
                     if self.dt_method is 'AB3':
                        properties_d['position_oold']   = original_position
                        properties_d['velocity_oold']   = substep_velocity
                      
            properties = properties_d.keys()
            particle_properties = dict((key, 0) for key in properties)
            has_properties = True
        else:
            has_properties = False    
            
        pmap = self.particle_map
        my_found = np.zeros(len(list_of_particles), 'I')
        all_found = np.zeros(len(list_of_particles), 'I')
        for i, particle in enumerate(list_of_particles):
            c = self.locate(particle)
            if not (c == -1 or c == __UINT32_MAX__):
                my_found[i] = True
                if not has_properties:
                    pmap += self.mesh, c, particle
                else:
                    # Get values of properties for this particle
                    for key in properties:
                        particle_properties[key] = properties_d[key][i]
                    pmap += self.mesh, c, particle, particle_properties
        # All particles must be found on some process
        comm.Reduce(my_found, all_found, root=0)

        if self.myrank == 0:
            missing = np.where(all_found == 0)[0]
            n_missing = len(missing)
            
            # Print particle info
            if self.__debug:
                for i in missing:
                    print 'Missing', list_of_particles[i].position

                n_duplicit = len(np.where(all_found > 1)[0])
                print 'There are %d duplicit particles' % n_duplicit
                
    def step(self,u,dt, stepnum = None):
        '''
        Move particles depending on integration method of choice'
        '''
        if self.dt_method is None or self.dt_method is 'Euler' :
            stop_shift, stop_reloc = self.__step_basis(u,dt)
        elif self.dt_method is 'RK2':
            stop_shift, stop_reloc = self.__step_basis(u,dt,step = 0, finalize = False)
            stop_shift, stop_reloc = self.__step_basis(u,dt,step = 1, weights = np.array([0.5,0.5]), finalize = True)
        elif self.dt_method is 'RK3':
                stop_shift, stop_reloc = self.__step_basis(u,0.5*dt,step = 0, finalize = False)
                stop_shift, stop_reloc = self.__step_basis(u,0.75*dt,step = 1, finalize = False)
                stop_shift, stop_reloc = self.__step_basis(u,dt,step = 2,weights = 1/9.*np.array([2.,3.,4.]),  finalize = True)
        elif self.dt_method is 'RK4':
                stop_shift, stop_reloc = self.__step_basis(u,0.5*dt,step = 0, finalize = False)
                stop_shift, stop_reloc = self.__step_basis(u,0.5*dt,step = 1, finalize = False)
                stop_shift, stop_reloc = self.__step_basis(u,dt,step = 2, finalize = False)
                stop_shift, stop_reloc = self.__step_basis(u,dt,step = 3,weights = 1/6.*np.array([1.,2., 2., 1.]),  finalize = True)
        elif self.dt_method is 'AB2':
            assert stepnum is not None, 'Stepnum must be provided for multistep Adams-Bashforth'
            stop_shift, stop_reloc = self.__step_ab_2(u,dt,stepnum)
        elif self.dt_method is 'AB3':
            assert stepnum is not None, 'Stepnum must be provided for multistep Adams-Bashforth'
            stop_shift, stop_reloc = self.__step_ab_3(u,dt,stepnum)
            
        return (stop_shift, stop_reloc)
                  
    def step_expression(self, u, dt):
        '''
        Move particles depending on integration method of choice
        for a given expression.
        '''
        if self.dt_method is None or self.dt_method is 'Euler':
            stop_shift, stop_reloc = self.__step_basis_exp(u,dt)
        elif self.dt_method is 'RK2':
            stop_shift, stop_reloc = self.__step_basis_exp(u,dt,step = 0, finalize = False)
            stop_shift, stop_reloc = self.__step_basis_exp(u,dt,step = 1, weights = np.array([0.5,0.5]), finalize = True)
        elif self.dt_method is 'RK3':
            stop_shift, stop_reloc = self.__step_basis_exp(u,0.5*dt,step = 0, finalize = False)
            stop_shift, stop_reloc = self.__step_basis_exp(u,0.75*dt,step = 1, finalize = False)
            stop_shift, stop_reloc = self.__step_basis_exp(u,dt,step = 2,weights = 1/9.*np.array([2.,3.,4.]),  finalize = True)
        else: 
            print('Method step_expression not implemented for chosen time stepping method, doing nothing.')
        
        return (stop_shift, stop_reloc)
               
    def relocate(self):
        # Relocate particles on cells and processors
        p_map = self.particle_map
        # Map such that map[old_cell] = [(new_cell, particle_id), ...]
        # Ie new destination of particles formerly in old_cell
        new_cell_map = defaultdict(list)
        for cwp in p_map.itervalues():
            for i, particle in enumerate(cwp.particles):
                point = df.Point(*particle.position)
                # Search only if particle moved outside original cell
                if not cwp.contains(point):
                    found = False
                    # Check neighbor cells
                    for neighbor in self.cell_neighbors[cwp.index()]:                     
                        if neighbor.contains(point):
                            new_cell_id = neighbor.index()
                            found = True
                            break
                    # Do a completely new search if not found by now
                    if not found:                       
                        # Fast detection if local process does contain point
                        new_cell_id = self.locate(particle)
                    # Record to map
                    new_cell_map[cwp.index()].append((new_cell_id, i))

        # Rebuild locally the particles that end up on the process. Some
        # have cell_id == -1, i.e. they are on other process
        list_of_escaped_particles = []
        for old_cell_id, new_data in new_cell_map.iteritems():
            # We iterate in reverse becasue normal order would remove some
            # particle this shifts the whole list!
            for (new_cell_id, i) in sorted(new_data,
                                           key=lambda t: t[1],
                                           reverse=True):
                particle = p_map.pop(old_cell_id, i)

                if new_cell_id == -1 or new_cell_id == __UINT32_MAX__ :
                    list_of_escaped_particles.append(particle)
                else:
                    p_map += self.mesh, new_cell_id, particle

        # Create a list of how many particles escapes from each processor
        self.my_escaped_particles[0] = len(list_of_escaped_particles)
        # Make all processes aware of the number of escapees
        comm.Allgather(self.my_escaped_particles, self.tot_escaped_particles)

        # Send particles to root
        if self.myrank != 0:
            for particle in list_of_escaped_particles:
                particle.send(0)

        # Receive the particles escaping from other processors
        if self.myrank == 0:
            for proc in self.other_processes:
                for i in range(self.tot_escaped_particles[proc]):
                    self.particle0.recv(proc)
                    list_of_escaped_particles.append(copy.deepcopy(self.particle0))

        # Put all travelling particles on all processes, then perform new search
        travelling_particles = comm.bcast(list_of_escaped_particles, root=0)
        self.add_particles(travelling_particles, call = 'to_process')

    def total_number_of_particles(self):
        'Return number of particles in total and on process.'
        num_p = self.particle_map.total_number_of_particles()
        tot_p = comm.allreduce(num_p)
        return (tot_p, num_p)

    def locate(self, particle):
        'Find mesh cell that contains particle.'
        assert isinstance(particle, (Particle, np.ndarray))
        if isinstance(particle, Particle):
            # Convert particle to point
            point = df.Point(*particle.position)
            return self.tree.compute_first_entity_collision(point)
        else:
            return self.locate(Particle(particle))
            
    def particle_contribution(self,functionspace ,pproperty_name,cell_index):
        '''
        Method returns the element particle matrix M_p and vector Chi_p \psi_p
        '''
        
        element         = functionspace.element()
        num_subspace    = element.num_sub_elements()
        space_dimension = element.space_dimension()
        coefficients    = np.zeros(space_dimension)
        
        ## Get number of dof points per cell:
        if num_subspace == 0:
            num_dof_points = space_dimension
        else:
            num_dof_points = space_dimension / num_subspace
        
        for cwp in self.particle_map.itervalues():
            cell_index_local = cwp.index()
            if cell_index_local == cell_index:
                if num_subspace == 0:
                    q = []; f = []
                    basis_matrix = np.zeros(space_dimension, dtype=float)
                else:
                    q = []; f = []
                    basis_matrix = np.zeros((space_dimension,num_subspace),\
                                    dtype=float)
                num_particles_cell =  len(cwp.particles)
                
                for particle in cwp.particles:
                    # Get position and property of particle
                    x = copy.deepcopy(particle.position)
                    p = copy.deepcopy(particle.properties[pproperty_name])
                    element.evaluate_basis_all(basis_matrix,
                                                x,
                                                cwp.get_vertex_coordinates(),
                                                cwp.orientation())
                    if num_subspace == 0:
                        q.append(copy.deepcopy(basis_matrix.T))
                        f.append(p)
                    else:
                        q.extend(copy.deepcopy(basis_matrix.T))
                        f.extend(p)
                q = np.asarray(q)
                
                # Get contributions and scale by number of particles
                M_p =  np.dot(q.T,q) * 1./float(len(f))
                f_p =  np.dot(q.T,f) * 1./float(len(f))
                   
                # We need an overdetermined system:
                #num_dof_locs = functionspace.element().space_dimension()                
                #if len(f) < num_dof_locs :
                   #print '(Under)Determined in cell', cell_index_local    
                  
                return M_p, f_p 
    
    def mesh_to_particles(self,function,property_name):
        '''
        Set/update particle property by full interpolation of function
        '''
        
        # Assert if property_name in properties!
        for cwp in self.particle_map.itervalues():
            for particle in cwp.particles:
                x = particle.position
                particle.properties[property_name] = function(x)
    
    def mesh_to_particles_flip(self, function_new, function_old, property_name, delta_property = None, step = None, theta = None):
        '''
        Update particle property by FLIP approach:
        
        psi_p^{n+1} = psi_p^{n} + theta * Delta phi^{n+1} + (1-theta) Delta phi^{n}
        
        where phi^{n+1} = function_new - function_old
        '''
        if theta is None:
            # Set theta = 1.0
            theta = 1.0
        else:
            assert delta_property is not None and step is not None, 'Incorrect input'
        assert theta >= 0.5 and theta <= 1.0, 'Theta out of bounds [0.5 , 1.0]'
        
        for cwp in self.particle_map.itervalues():
            for particle in cwp.particles:
                x = particle.position
                if step == 0 or abs(theta - 1.0) < 1E-12:
                    dpsi = function_new(x) - function_old(x)
                    particle.properties[property_name] += dpsi
                    if abs(theta - 1.0) > 1E-12:
                        particle.properties[delta_property] = dpsi
                else:
                    dpsi  = function_new(x) - function_old(x)
                    particle.properties[property_name] += \
                        theta*dpsi + (1.-theta) * particle.properties[delta_property] 
                    particle.properties[delta_property] = dpsi
    
    def pdistribution(self, num_cells, num_particles):
        '''
        Particle distribution regularity metric following 
        Maljaars (2018)
        '''
        
        Sbar = num_particles / float(num_cells)
        delta_S = 0.
        for cwp in self.particle_map.itervalues():
           St = len(cwp.particles)
           delta_S += abs( (St - Sbar) / Sbar )
        delta_S *= 1./float(num_cells)
        return delta_S
    
    def scatter(self, fig, skip=1,axis_lim = None):
        'Scatter plot of all particles on process 0'
        import matplotlib.colors as colors
        import matplotlib.cm as cmx

        ax = fig.gca()
        if axis_lim is None:
            ax.axis([0,1.,0.,1.])
        else:
            ax.axis(axis_lim)
        p_map = self.particle_map
        all_particles = np.zeros(self.num_processes, dtype='I')
        my_particles = p_map.total_number_of_particles()
        # Root learns about count of particles on all processes
        comm.Gather(np.array([my_particles], 'I'), all_particles, root=0)
        
        up = []
        for cwp in self.particle_map.itervalues():
            for particle in cwp.particles:
                up.append(particle.properties['Scalar'])
        
        # Slaves should send to master
        if self.myrank > 0:
            for cwp in p_map.itervalues():
                for p in cwp.particles:
                    p.send(0)
        else:
            # Receive on master
            received = defaultdict(list)
            received[0] = [copy.copy(p.position)
                           for cwp in p_map.itervalues()
                           for p in cwp.particles]
            for proc in self.other_processes:
                # Receive all_particles[proc]
                for j in range(all_particles[proc]):
                    self.particle0.recv(proc)
                    received[proc].append(copy.copy(self.particle0.position))

            cmap = cmx.get_cmap('jet')
            cnorm = colors.Normalize(vmin=0, vmax=self.num_processes)
            scalarMap = cmx.ScalarMappable(norm=cnorm, cmap=cmap)

            for proc in received:
                # Plot only if there is something to plot
                particles = received[proc]
                if len(particles) > 0:
                    xy = np.array(particles)
                    ax.scatter(xy[::skip, 0], xy[::skip, 1],
                               label='%d' % proc,
                               c=up,
                               edgecolor='none')
            ax.legend(loc='best')
    
    
    def save(self, filename_1, filename_2,property_name):
        '''
        Pickle particle positions/particle velocities
        '''
        xp = []
        up = []
        for cwp in self.particle_map.itervalues():
            for particle in cwp.particles:
                xp.append(particle.position)
                up.append(particle.properties[property_name])
        xp = np.asarray(xp)
        up = np.asarray(up)
        
        xp_p0 = comm.gather(xp, root = 0)
        up_p0 = comm.gather(up, root = 0)
        
        if comm.Get_rank() == 0: 
            xp_p0 = np.vstack(xp_p0)
            up_p0 = np.vstack(up_p0)
            with open(filename_1,  "wb") as f:
                pickle.dump(xp_p0,f)
                print xp_p0
                
            with open(filename_2,  "wb") as f:
                pickle.dump(up_p0,f)
    
   
    #### PRIVAVE METHODS
    def __step_basis(self,u,dt,step = None, weights=None,finalize = True):
        '''
        Basis method for multi-stage Runge-Kutta schemes
        '''
  
        start = df.Timer('shift')
        for cwp in self.particle_map.itervalues():
            # Restrict once per cell
            u.restrict(self.coefficients,
                       self.element,
                       cwp,
                       cwp.get_vertex_coordinates(),
                       cwp)
            for particle in cwp.particles:
                x = particle.position
                # Compute velocity at position x 
                if step == 0: 
                    particle.properties['position_old'] = copy.deepcopy(x[:])
                self.element.evaluate_basis_all(self.basis_matrix,
                                                x,
                                                cwp.get_vertex_coordinates(),
                                                cwp.orientation())
                
                if step is not None and finalize is False:
                    particle.properties['velocity_sub'][step] =  np.dot(self.coefficients, self.basis_matrix)[:]
                    x[:] = particle.properties['position_old'] + dt*particle.properties['velocity_sub'][step]
                elif step is not None and finalize is True:
                    particle.properties['velocity_sub'][step] = np.dot(self.coefficients, self.basis_matrix)[:]
                    x[:]= particle.properties['position_old'] + dt*np.dot(weights,particle.properties['velocity_sub'])
                else:
                    x[:] = x[:] + dt*np.dot(self.coefficients, self.basis_matrix)[:]
                
                # Apply periodic boundary condition 
                if self.periodic_bc is not None:
                    xbef = copy.deepcopy(x)
                    self.__apply_periodic(x)
                    xaft = copy.deepcopy(x)
                    
                    # Optionally reset particle scalar value 
                    if self.reset_value is not None:
                        deltax = xaft - xbef
                        if np.linalg.norm(deltax) > 1E-6:
                            if isinstance(self.reset_value,list) and len(self.reset_value) is 3:
                                if  (xaft[0]) / (xaft[1]) < self.reset_value[0] / self.reset_value[1]:
                                    particle.properties['Scalar'] = 0.
                                else:   
                                    particle.properties['Scalar'] = self.reset_value[2]
                            elif self.reset_value == 'Velocity':
                                # This probably only works in serial! 
                                #print xaft, xbef
                                particle.properties['Velocity'] = u(xaft)
                                if particle.properties['deltaVel'] is not None:
                                    particle.properties['deltaVel'] *= 0.
                    
        # Recompute the map
        stop_shift = start.stop()
        start =df.Timer('relocate')
        info = self.relocate()
        stop_reloc = start.stop()
        return (stop_shift, stop_reloc)
    
    def __step_ab_2(self,u,dt,stepnum):
        '''
        Private method for handling Adams-Bashforth 2
        Method is initialized with Euler forward step.
        '''
        
        start = df.Timer('shift')
        for cwp in self.particle_map.itervalues():
            # Restrict once per cell
            u.restrict(self.coefficients,
                        self.element,
                        cwp,
                        cwp.get_vertex_coordinates(),
                        cwp)
            for particle in cwp.particles:
                x = particle.position
                # Compute velocity at position x    
                self.element.evaluate_basis_all(self.basis_matrix,
                                                x,
                                                cwp.get_vertex_coordinates(),
                                                cwp.orientation())
                upn = np.dot(self.coefficients, self.basis_matrix)[:] 
                upo   = particle.properties['velocity_sub']
                if stepnum == 0:
                    x += dt * upn                     
                else:
                    x += 0.5 * dt * (3.*upn - upo)
                
                if self.periodic_bc is not None:                   
                    self.__apply_periodic(x)
                    
                # Copy values into old  
                particle.position = x
                particle.properties['velocity_sub'] = copy.deepcopy(upn)
        
        stop_shift = start.stop()
        start =df.Timer('relocate')
        info = self.relocate()
        stop_reloc = start.stop()
        return (stop_shift, stop_reloc)
    
    def __step_ab_3(self,u,dt,stepnum):
        '''
        Private method for handling Adams-Bashforth 3
        Method is initialized with Euler forward step, 
        followed by Adams-Bashforth 2 step.
        '''
        start = df.Timer('shift')
        for cwp in self.particle_map.itervalues():
            # Restrict once per cell
            u.restrict(self.coefficients,
                        self.element,
                        cwp,
                        cwp.get_vertex_coordinates(),
                        cwp)
            for particle in cwp.particles:
                x = particle.position
                # Compute velocity at position x    
                self.element.evaluate_basis_all(self.basis_matrix,
                                                x,
                                                cwp.get_vertex_coordinates(),
                                                cwp.orientation())
                upn = np.dot(self.coefficients, self.basis_matrix)[:] 
                upint  = copy.deepcopy(particle.properties['velocity_sub'])
                upo   = particle.properties['velocity_sub']
                upoo   = particle.properties['velocity_oold']
                
                if stepnum == 0:
                    x += dt * upn
                elif stepnum == 1:
                    x += 0.5 * dt * (3.*upn - upo)
                else:
                    x += dt * ( 23./12. * upn \
                                - 4./3. * upo \
                                + 5./12.* upoo) 
                
                if self.periodic_bc is not None:                   
                    self.__apply_periodic(x)
                
                # Copy values into old  
                particle.position = x
                particle.properties['velocity_sub']  = copy.deepcopy(upn)
                particle.properties['velocity_oold'] = copy.deepcopy(upint)
                
        stop_shift = start.stop()
        start =df.Timer('relocate')
        info = self.relocate()
        stop_reloc = start.stop()
        return (stop_shift, stop_reloc)
            
    def __step_basis_exp(self,u,dt,step = None, weights=None,finalize = True):
        '''
        Private basis method for time integrating expressions
        '''
        
        start = df.Timer('shift')
        for cwp in self.particle_map.itervalues():
            for particle in cwp.particles:
                x = particle.position
                # Compute velocity at position x 
                if step == 0: 
                    particle.properties['position_old'] = copy.deepcopy(x[:])
              
                if step is not None and finalize is False:
                    particle.properties['velocity_sub'][step] =  u(x)
                    x[:] = particle.properties['position_old'] + dt*particle.properties['velocity_sub'][step]
                elif step is not None and finalize is True:
                    particle.properties['velocity_sub'][step] = u(x)
                    x[:]= particle.properties['position_old'] + dt*np.dot(weights,particle.properties['velocity_sub'])
                else:
                    x[:] = x[:] + dt*u(x)
                if self.periodic_bc is not None:
                    self.__apply_periodic(x)
                    
        # Recompute the map
        stop_shift = start.stop()
        start =df.Timer('relocate')
        info = self.relocate()
        stop_reloc = start.stop()
        return (stop_shift, stop_reloc)
       
    def __apply_periodic(self, xp):
        '''
        Private method for applying periodic bc's
        '''
        
        if len(self.periodic_bc) == 4:
            if xp[0] < self.periodic_bc[0,0]:
                xp[0] += self.periodic_bc[1,0] -  self.periodic_bc[0,0]
                xp  = self.__check_ybound(xp)
            elif xp[0] > self.periodic_bc[1,0]:
                xp[0] +=  self.periodic_bc[0,0] - self.periodic_bc[1,0] 
                xp  = self.__check_ybound(xp)
            elif xp[1] < self.periodic_bc[2,2]:
                xp[1] += self.periodic_bc[3,2] -  self.periodic_bc[2,2]
                xp    = self.__check_xbound(xp)
            elif xp[1] > self.periodic_bc[3,2]:
                xp[1] +=  self.periodic_bc[2,2] - self.periodic_bc[3,2]
                xp    = self.__check_xbound(xp)
        elif len(self.periodic_bc) == 2:
            if xp[0] < self.periodic_bc[0,0]:
                xp[0] += self.periodic_bc[1,0] -  self.periodic_bc[0,0]
            elif xp[0] > self.periodic_bc[1,0]:
                xp[0] +=  self.periodic_bc[0,0] - self.periodic_bc[1,0] 
        return xp
            
    def __check_xbound(self,xp):
        if xp[0] < self.periodic_bc[0,0]:
            xp[0] += self.periodic_bc[1,0] -  self.periodic_bc[0,0]
        elif xp[0] > self.periodic_bc[1,0]:
            xp[0] +=  self.periodic_bc[0,0] - self.periodic_bc[1,0]
            
    def __check_ybound(self,xp):
        if xp[1] < self.periodic_bc[2,2]:
            xp[1] += self.periodic_bc[3,2] -  self.periodic_bc[2,2]
        elif xp[1] > self.periodic_bc[3,2]:
            xp[1] +=  self.periodic_bc[2,2] - self.periodic_bc[3,2]
        return xp                                         
