__author__ = "Jakob Maljaars <j.m.maljaars@tudelft.nl>"
__date__ = "2018-02-06"
__copyright__ = "Copyright (C) 2018 " + __author__
__license__ = "GNU Lesser GPL version 3 or any later version"

from dolfin import *
import numpy as np 
from LagrangianParticles import LagrangianParticles
from mpi4py import MPI as pyMPI

__all__ = ['FormsPDEMap', 'PDEMap']

comm = pyMPI.COMM_WORLD

class FormsPDEMap:
    """
    This class initializes (and returns) 
    the forms related to the PDE-constrained 
    particle-mesh interaction.
    For symbol definitions, refer to MANUSCRIPT.
    """
    
    def __init__(self, mesh,FuncSpace_dict, beta_map = Constant(1E-6), ds = ds):
        self.W     = FuncSpace_dict['FuncSpace_local'] 
        self.T     = FuncSpace_dict['FuncSpace_lambda']
        self.Wbar  = FuncSpace_dict['FuncSpace_bar']
        
        self.n = FacetNormal(mesh)
        self.beta_map = beta_map
        self.ds = ds
        self.form_dict = None
            
    def forms_theta_linear(self,psih0, uh, dt, theta_map, \
                           theta_L = Constant(1.0), dphi0 = Constant(0.), dphi00 = Constant(0.),
                           h = Constant(0.), neumann_idx = 99):
        psi    ,  w    = TrialFunction(self.W)  , TestFunction(self.W)
        lamb   ,  tau  = TrialFunction(self.T)  , TestFunction(self.T)
        psibar ,  wbar = TrialFunction(self.Wbar), TestFunction(self.Wbar)
        
        beta_map = self.beta_map; n = self.n 
        facet_integral = self.facet_integral
         
        # Define psi_star 
        psi_star =  psih0 + (1-theta_L)*dphi00 + theta_L * dphi0 
        # Switch to detect in/outflow boundary
        gamma    =  conditional( ge(dot(uh,n), 0), 0, 1)
                
        N_a = facet_integral(beta_map*dot(psi,w))
        G_a = dot(lamb,w)/dt *dx - theta_map * dot(uh,grad(lamb))*w*dx \
               + theta_map * (1-gamma) * dot(uh,n) * lamb * w * self.ds(neumann_idx) 
        
        L_a = -facet_integral(beta_map * dot(psibar, w)) 
        H_a = facet_integral(dot(uh,n)*psibar* tau) \
              - dot(uh,n)*psibar* tau * self.ds(neumann_idx)
        B_a = facet_integral(beta_map * dot(psibar ,wbar)) 
        
        # RHS contributions
        Q_a = dot( Constant(0), w) * dx 
        R_a = dot(psi_star,tau)/dt*dx  \
                + (1-theta_map)*dot(uh,grad(tau))*psi_star*dx  \
                - (1-theta_map) * (1-gamma) * dot(uh,n) * psi_star * tau * self.ds(neumann_idx) \
                + gamma*dot(h,tau) * self.ds(neumann_idx)    
        S_a = facet_integral( Constant(0) * wbar) 
        return self.__get_form_dict(N_a, G_a, L_a, H_a, B_a, Q_a, R_a, S_a)
                
    def forms_theta_nlinear(self, v0, Ubar0, dt, theta_map, 
                            theta_L = Constant(1.0), duh0 = Constant((0.,0.)), duh00 = Constant((0.,0)),
                            h = Constant((0.,0.)), neumann_idx = 99):  
        
        # Define trial test functions 
        v, w        = TrialFunction(self.W), TestFunction(self.W)
        lamb, tau   = TrialFunction(self.T), TestFunction(self.T)
        vbar, wbar  = TrialFunction(self.Wbar), TestFunction(self.Wbar)
        
        beta_map = self.beta_map; n = self.n 
        facet_integral = self.facet_integral
        
        # Define v_star
        v_star = v0 + (1-theta_L) * duh00 + theta_L * duh0
        # Switch to detect in/outflow boundary
        gamma  = conditional( ge(dot(v_star,n), 0), 0, 1)
        
        Udiv   = v0 + duh0          
        outer_v_a    = outer(w     , Udiv) 
        outer_v_a_o  = outer(v_star, Udiv) 
        outer_ubar_a = outer(vbar  , Ubar0)
          
        # LHS contribution s
        N_a  = facet_integral(beta_map*dot(v,w))        
        G_a  = dot(lamb,w)/dt *dx - theta_map*inner(outer_v_a , grad(lamb))*dx \
               + theta_map * (1-gamma) * dot(outer_v_a * n, lamb) * self.ds(neumann_idx)         
                  
        L_a  = -facet_integral(beta_map * dot(vbar, w)) 
        H_a  = facet_integral(dot(outer_ubar_a*n,tau)) \
               - dot(outer_ubar_a*n,tau) * self.ds(neumann_idx)
        B_a  = facet_integral(beta_map * dot(vbar ,wbar))
        
        # RHS contributions
        Q_a = dot( Constant((0,0)), w) * dx 
        R_a = dot(v_star, tau)/dt * dx \
                + (1-theta_map)*inner(outer_v_a_o, grad(tau))*dx \
                + gamma * dot(h,tau) * self.ds(neumann_idx)    
        S_a = facet_integral( dot( Constant((0,0)), wbar))
                
        return self.__get_form_dict(N_a, G_a, L_a, H_a, B_a, Q_a, R_a, S_a)
        
        
    # Short-cut function for evaluating sum_{K} \int_{K} (integrand) ds
    def facet_integral(self, integrand):
        return integrand('-')*dS + integrand('+')*dS + integrand*ds
    
    def __get_form_dict(self, N_a, G_a, L_a, H_a, B_a, Q_a, R_a, S_a):
        # Convert into forms
        N_a = Form(N_a); G_a = Form(G_a); L_a = Form(L_a); 
        H_a = Form(H_a); B_a = Form(B_a)
        Q_a = Form(Q_a); R_a = Form(R_a); S_a = Form(S_a)
        
        # Return dict of (ufl) forms
        return {'N_a': N_a, 'G_a': G_a, 'L_a': L_a, 'H_a': H_a, 'B_a': B_a,
                'Q_a': Q_a, 'R_a': R_a, 'S_a': S_a}   
    
class PDEMap:
    """
    PDEMap class contains the essential tools to perform the 
    PDE-constrained particle-mesh interaction, using a
    static condensation strategy. 
    The class expects the relevant forms to be provided, 
    corresponding to the algebraic problem:
        
    |  N   G   L  | |  Psi      |     |  Q  |
    |  G^T 0   H  | |  Lamb     |  =  |  R  |
    |  L^T H^T B  | |  Psi_bar  |     |  S  |
        
    """
    def __init__(self, mesh, N, G, L, H, B, Q, R, S):
        self._assert_input(N,G,L,H,B,Q,R,S)
        
        # Set info 
        self.N = N; self.G = G; self.L = L; self.H = H; self.B = B
        self.Q = Q; self.R = R; self.S = S
        
        self.mesh = mesh
        
        # Get cell-dof lists
        self.c2d_local = self._dof_properties(mesh, N.function_space(0))
        # TODO: put assertion on function space dimensions for form G
        self.c2d_lm    = self._dof_properties(mesh, G.function_space(1))
        self.c2d_bar   = self._dof_properties(mesh, B.function_space(0))
    
    def update_forms(self, N, G, L, H, B, Q, R, S):
        self._assert_input(N,G,L,H,B,Q,R,S)
        
        # Set info 
        self.N = N; self.G = G; self.L = L; self.H = H; self.B = B
        self.Q = Q; self.R = R; self.S = S

    def assemble(self, particles, quantity_name, bc_list = None, ignore_empty=False):
        """
        Assemble the global system for PDE constrained 
        particle-mesh projection
        """
        
        # Dummy assemble to obtain matrices with correct sparsity pattern
        A_g = assemble(self.B)
        f_g = assemble(self.S)
        A_g.zero()
        f_g.zero()
        
        # Initialize empty lists for later use 
        invKSlist = []; LHlist = []; QRlist = []
        empty_cell = False
        # Loop over cells 
        for i,cell in enumerate(cells(self.mesh)):           
            # Get the individual local contributions
            N_e = assemble_local(self.N,cell)
            G_e = assemble_local(self.G,cell)
                        
            H_e = assemble_local(self.H,cell)
            L_e = assemble_local(self.L,cell)
            B_e = assemble_local(self.B,cell)
            
            Q_e = assemble_local(self.Q,cell)
            R_e = assemble_local(self.R,cell)
            S_e = assemble_local(self.S,cell)
            
            # Get particle contributions
            try:
                Mp_e, fp_e = particles.particle_contribution(self.N.function_space(0),quantity_name,i)
            except:
                if not ignore_empty:
                    # Then raise error if empty cell present
                    raise Exception('Cell '+str(i)+' does not contain particles')
                else:
                    # Then make particle contributions equal to 0
                    Mp_e = np.eye(np.shape(N_e)[0], dtype = np.float)
                    fp_e = np.zeros(np.shape(Q_e),dtype = np.float)
                    # But raise warning:
                    print('Empty cell '+str(i)+' on process '+str(comm.Get_rank())+' ignored.')
                    empty_cell = True                    
            
            # Invert local contribution
            ncol = np.shape(G_e)[1]
            
            try:
                invKS = np.linalg.inv( np.asarray( np.bmat([[Mp_e + N_e, G_e],
                                                            [G_e.T, np.zeros((ncol,ncol))]]) ) )                
            except:
                empty_cell = True
                print('Matrix singularity encountered, computation should quit gracefully') 
                invKS      = 0. # Make computation quit gracefully
                
            invKSlist.append(invKS)
                        
            LH = np.asarray(np.bmat([[L_e], [H_e] ]))
            QR = np.hstack((Q_e + fp_e , R_e))

            LHlist.append(LH)
            QRlist.append(QR)
            
            # Complete local contribution
            LHS_e =  LH.T.dot(invKS).dot(LH) - B_e
            RHS_e =  -S_e + LH.T.dot(invKS).dot(QR)
            
            # Assemble local contributions into global system
            idof_qb = np.asarray(self.c2d_bar[i], dtype = np.intc)
            A_g.add_local(LHS_e,idof_qb,idof_qb)
            f_g.add_local(RHS_e,idof_qb)
            
        # Finalize LHS and RHS
        A_g.apply("add")
        f_g.apply("add")
        
        # Set attributes
        self.A = A_g; self.b = f_g
        self.invKSlist = invKSlist; self.QRlist = QRlist
        self.LHlist    = LHlist
        
        if bc_list is not None:
            self._apply_bc(bc_list)
        return empty_cell
    
    def lstsq_map(self, particles, quantity_name, function_local):
        """
        Simple (local) least square projection, 
        similar to Maljaars (2018)
        """
        for i,cell in enumerate(cells(self.mesh)):
            try:
                Mp_e, fp_e = particles.particle_contribution(self.N.function_space(0),quantity_name,i)
            except:
                raise Exception('Cell '+str(i)+' does not contain particles')
                        
            idof_q1   = np.asarray(self.c2d_local[i],dtype=np.intc)
            local_vec = np.linalg.solve(Mp_e, fp_e)
            function_local.vector().set_local(local_vec, idof_q1)
    
    def solve_global(self, function_bar):
        # Solve global system for the bar function
        assert function_bar.function_space() == self.B.function_space(0), 'Wrong function space!'
        solve(self.A, function_bar.vector(),self.b)
    
    def backsubstitute(self, function_bar, function_local, function_lm = None):
        """
        Perform backsubtitution to obtain local unknowns 
        (and optionally the Lagrange multiplier function)
        """
        assert function_bar.function_space() == self.B.function_space(0), 'Wrong global function space!'
        assert function_local.function_space() == self.N.function_space(0), 'Wrong local function space!'
        if function_lm is not None:
            assert function_lm.function_space() == self.G.function_space(1), 'Wrong local function space!'
        
        for i,cell in enumerate(cells(self.mesh)):
            idof_q1 = np.asarray(self.c2d_local[i],dtype=np.intc)
            idof_q2 = np.asarray(self.c2d_lm[i],dtype=np.intc)
            idof_qb = np.asarray(self.c2d_bar[i],dtype=np.intc)
             
            bar_vec = np.zeros(len(idof_qb), dtype=np.float)
            function_bar.vector().get_local(bar_vec, idof_qb )
            local_vec = self.invKSlist[i].dot(self.QRlist[i] - self.LHlist[i].dot( bar_vec ) )
            
            function_local.vector().set_local( local_vec[0:len(idof_q1)], idof_q1 ) 
            if function_lm is not None:
                function_lm.vector().set_local( local_vec[len(idof_q1)::], idof_q2 ) 
                
        
    def _dof_properties(self, mesh, function_space):
        """
        Make array of dof numbers per cell 
        (array has dimension (fspace_dim*ndofs) x n_cells)
        """
        dofmap  =       function_space.dofmap()
        cell_to_dof = [ [dof for dof in dofmap.cell_dofs(cell)] \
                        for cell in range(mesh.num_cells())]
        np.asarray(cell_to_dof)
        return cell_to_dof
    
    def _assert_input(self, N, G, L, H, B, Q, R, S):
        assert type(N) is dolfin.fem.form.Form \
               and type(G) is dolfin.fem.form.Form \
               and type(L) is dolfin.fem.form.Form \
               and type(H) is dolfin.fem.form.Form \
               and type(B) is dolfin.fem.form.Form \
               and type(Q) is dolfin.fem.form.Form \
               and type(R) is dolfin.fem.form.Form \
               and type(S) is dolfin.fem.form.Form, \
               'Wrong input type, must be dolfin.fem.form.Form' 
        assert N.rank() is 2 and G.rank() is 2 and L.rank() is 2 \
            and H.rank() is 2 and B.rank() is 2, 'LHS contribution is of wrong rank'
        assert Q.rank() is 1 and R.rank() is 1 and S.rank() is 1, \
            'LHS contribution is of wrong rank'
    
    def _apply_bc(self, bc_list):
        for bc in bc_list:
            bc.apply(self.A, self.b)