"""
    Script for running the skew advection test
"""

from dolfin import *
from mshr import *
from mpi4py import MPI as pyMPI
import time as tm
import numpy as np 
import matplotlib.pyplot as plt

# Import Particle functionality
from LEoPart import *

comm = pyMPI.COMM_WORLD

################### INPUT BLOCK ###################
outdir  = "./Results/SkewAdvection/"

nx    = 25
k     = 1
p_res = 160

xmin,ymin = 0., 0.
xmax,ymax = 1., 1.
angle_deg_list = [15, 30, 45, 60]
h       = 1. 
Tend    = 2.0
dt      = Constant(0.02)
num_steps = np.rint(Tend/float(dt))
# Simple explicit Euler particle advection is sufficient for this test
advection_scheme = 'Euler' 

# Update particle values by mesh solution?
update_particle_values = False

theta     = Constant(1.) 
beta_stab = Constant(1e-6)

phi_exact = Expression('0',degree = 1)
###################

# Mark boundaries
class Left(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and near( x[0], 0. ) 

class Right(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and near(x[0],1.) #

class Upper(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and near(x[1],1.)  

class Lower(SubDomain):
    def inside(self,x,on_boundary):
        return on_boundary and near(x[1],0.)

def assign_particle_values(x, phi_exact):
    if comm.Get_rank() == 0:
        s=np.asarray([phi_exact(x[i,:]) for i in range(len(x))], dtype = np.float_)
    else:
        s = None
    return s

mesh = UnitSquareMesh(nx,nx)
n    = FacetNormal(mesh)

boundaries = FacetFunction("size_t", mesh) 
boundaries.set_all(0)
lowerbound = Lower()
upperbound = Upper()
leftbound  = Left()
rightbound = Right()

lowerbound.mark(boundaries,1)
leftbound.mark(boundaries, 2)
upperbound.mark(boundaries,99)
rightbound.mark(boundaries,99)
ds = Measure('ds', domain=mesh, subdomain_data=boundaries)

# Define function spaces
W      = FunctionSpace(mesh, 'DG', k)
T      = FunctionSpace(mesh, 'DG', 0)
Wbar   = FunctionSpace(mesh, 'DGT',k)
Vadv   = VectorFunctionSpace(mesh, 'DG', 5)

# Boundary conditions on left and lower part of domain
bc1 = DirichletBC(Wbar, Constant(0.),Left()  )
bc2 = DirichletBC(Wbar, Constant(h), Lower() )
bcs = [bc1, bc2]

for angle_deg in angle_deg_list:
    if comm.Get_rank() == 0:
        print ('{:=^72}'.format('Starting computation for angle '+str(angle_deg)))
    
    outfile = File(outdir+'phih_angle'+str(int(angle_deg))+'.pvd')
    angle = angle_deg* np.pi /180.
    ux, uy = np.cos(angle), np.sin(angle)
    
    uh        = Expression(('ux','uy'), ux = ux, uy = uy, degree=3) 
    reset_value = [ux,uy,h]
    
    psih, psih0       = Function(W), Function(W)
    psibarh, phibarh0 = Function(Wbar), Function(Wbar)
    lambh             = Function(T)
    psih0.assign(phi_exact)

    # Initialize PDE constrained map
    FuncSpace_adv  = {'FuncSpace_local': W, 'FuncSpace_lambda': T , 'FuncSpace_bar': Wbar}
    forms_adv      = FormsPDEMap(mesh, FuncSpace_adv, ds = ds).forms_theta_linear(psih0, uh, dt, theta)
    
    pde_projection = PDEMap(mesh,   forms_adv['N_a'], forms_adv['G_a'], forms_adv['L_a'],
                                                                        forms_adv['H_a'], 
                                                                        forms_adv['B_a'],
                                    forms_adv['Q_a'], forms_adv['R_a'], forms_adv['S_a'])
  
    uadvect = interpolate(uh,Vadv)
    x       = RandomRectangle(Point(xmin,ymin), Point(xmax,ymax), seed = 20).generate([p_res, p_res])
    s       = assign_particle_values(x, phi_exact)
    x = comm.bcast(x, root=0)
    s = comm.bcast(s, root=0)

    # Define particles
    periodic = np.array([[xmin,xmin, ymin, ymax],[xmax, xmax, ymin, ymax],\
                        [xmin,xmax, ymin,ymin ],[xmin, xmax, ymax, ymax]])
    lp = LagrangianParticles(Vadv, advection_scheme, periodic, reset_value)
    lp.add_particles(x, properties_d = {'Scalar': s})
    lp.mesh_to_particles(psih0,'Scalar')

    # Print some info
    if comm.Get_rank() == 0: 
        print("Num cells "+str(mesh.num_cells()))
        print("Num particles"+str(len(x)))
    
    step = 0
    time = 0.
    while step < num_steps:
        step += 1
        time += float(dt)
        if comm.Get_rank() == 0: print("Step: "+str(step)+", Time: "+str(time))
        invKSlist = []; LHlist = []; QRlist = []
        
        shift_t, rlocate_t = lp.step(uadvect, dt=float(dt))
        
        # Perform projection
        pde_projection.assemble(lp, 'Scalar', bcs)
        pde_projection.solve_global(psibarh)
        pde_projection.backsubstitute(psibarh, psih)
        
        if update_particle_values:
            lp.mesh_to_particles(psih,'Scalar')
        
        # Global conservation statement
        global_cons     = (psih - psih0)/dt * dx + dot(uh,n)*psibarh * ds(1) + dot(uh,n)* psibarh * ds(2) \
                            + theta * dot(uh,n)*psih * ds(99) + (1-theta ) * dot(uh,n)*psih0 * ds(99) 
        cons_statement  = assemble(global_cons) 
        if comm.Get_rank() == 0: 
            print("Global Conservation? "+str(cons_statement))
        assign(psih0, psih)
        
        # Save result
        outfile << psih
        
    dofcoords_q =  W.tabulate_dof_coordinates().reshape((-1,2))

    phih_vec  = psih.vector().array()
    phihb_vec = psibarh.vector().array()

    dofcoords_g = comm.gather(dofcoords_q, root = 0)
    phih_gather = comm.gather(phih_vec, root = 0)
    phihb_gather = comm.gather(phihb_vec, root = 0)

    if comm.Get_rank()  == 0 :
        dofcoords_g = np.vstack(dofcoords_g)
        phih_gather = np.hstack(phih_gather)
        idcs = np.argsort(phih_gather)
        phihb_gather =np.hstack(phihb_gather) 
    
        print('Min/max value:')
        print(np.sort(phih_gather)[0:5], np.sort(phih_gather)[-5::])
        print('Min Coords:') 
        print(dofcoords_g[idcs[0:5]])
        print('Max coords:') 
        print(dofcoords_g[idcs[-5::]])
        print('Max/min value bar:') 
        print(np.sort(phihb_gather)[0:5], np.sort(phihb_gather)[-5::])
        
    dofcoords =  Wbar.tabulate_dof_coordinates().reshape((-1,2))
    sigmabar_pt = psibarh.vector().array()

    dofcoords_gather = comm.gather(dofcoords, root = 0)
    phibar_gather    = comm.gather(sigmabar_pt, root = 0)


