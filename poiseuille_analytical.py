from dolfin import *
import numpy as np

# Analytical solution (series expansion)
def fourier_component(i,y,force,nu,d,time):
    'Fourier coefficients for analytical equilibrium solution.'
    result = (-1)**i*16*d**2*force/(nu*np.pi**3*(2*i+1)**3)* \
            np.cos((2*i+1)*np.pi*y/(2*d))* \
            np.exp(-1*((2*i+1)**2*np.pi**2*nu*time / (4*d**2))) 
    return result

def T_e(x, y, force,nu,d,time):
    'Analytical equilibrium solution'
    assert x.shape == y.shape
    N = 50
    ue = np.zeros(x.shape)
    ue[0::2] = force/(2*nu)*(d**2-y[1::2]**2)
    for i in range(0, N):
        ue[0::2] -= fourier_component(i,y[1::2],force,nu,d,time)    
    return ue

class AnalyticalSolution(object):
    'Analytical solution to poiseuille problem as function in V'
    def __init__(self, V, force,nu,d,time):
        'Precompute the equilibrium part of the solution.'
        self.f = Function(V)

        dofmap = V.dofmap()
        mesh = V.mesh()
        dof_x = V.tabulate_dof_coordinates().reshape((-1, 2))
        x, y = dof_x[:, 0], dof_x[:, 1]
        # Assign computed temperature
        self.f.vector()[:] = T_e(x, y, force,nu,d,time)
    
    def __call__(self):
        'Add the time dependent part and return complete solution.'
        return self.f