"""
    Script for running the Taylor-Green flow convergence test
"""

from dolfin import *
#from ParticleGenerator import *
from mpi4py import MPI as pyMPI
import numpy as np
import os
##
from LEoPart import *
from StaticCondensation import FormsStokes, Stokes_StaticCondensation
##

comm = pyMPI.COMM_WORLD

### Input block
outdir_base      = './Results/Taylor_Green/'
xmin = -1.; xmax = 1.
ymin = -1.; ymax = 1.

factor_list = [1, 2, 4, 8] 
nxbase   = 8
pres_base= 60
nu_list = [Constant(2E-2), Constant(2E-3)]*3
dt_base = 8e-2
klist  = [1, 1, 2, 2, 2, 2]; 
llist  = [0]*6; 
kbarlist = klist
methodlist = ['pde']*4 + ['lstsq']*2

Tend       = 2.0
beta       = Constant(1e-6)
theta_map  = Constant(0.5)
advection_scheme = 'AB2'

# Body force
f = Constant((0.,0.))

# Overload SubDomain class to define PeriodicBoundary
boundary_dict = dict(xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)

class PeriodicBoundary(SubDomain):
    # Left boundary is "target domain" G
    def __init__(self,bdict):
        SubDomain.__init__(self)
        self.xmin, self.xmax = bdict['xmin'], bdict['xmax']
        self.ymin, self.ymax = bdict['ymin'], bdict['ymax']
        
    def inside(self, x, on_boundary):
        # return True if on left or bottom boundary AND NOT on one of the two corners (0, 1) and (1, 0)
        return bool((near(x[0], self.xmin) or near(x[1], self.ymin)) and\
                    (not ((near(x[0], self.xmin) and near(x[1], self.ymax)) or 
                    (near(x[0], self.xmax) and near(x[1], self.ymin)))) and on_boundary)

    def map(self, x, y):
        if near(x[0], self.xmax) and near(x[1], self.ymax):
            y[0] = x[0] - (self.xmax - self.xmin)
            y[1] = x[1] - (self.ymax - self.ymin)
        elif near(x[0], self.xmax):
            y[0] = x[0] - (self.xmax - self.xmin)
            y[1] = x[1]
        else:   # near(x[1], 1)
            y[0] = x[0]
            y[1] = x[1] - (self.ymax - self.ymin)
            
def Corner(x, on_boundary):
    return near(x[0], 1) and near(x[1], 1)

for i,nu in enumerate(nu_list):
    k = klist[i]; l = llist[i]; kbar = kbarlist[i]
    method = methodlist[i]
    
    outdir      = outdir_base+'nu'+str(float(nu))+'/Case_k'+str(k)+'l'+str(l)+'kbar' \
                        +str(kbar)+'_'+method+'/'
    error_table = outdir+'error_table' 
    
    try:
        os.remove(error_table)
    except OSError:
        pass
    
    for factor in factor_list:
        nx = nxbase  * factor ; ny = nxbase  * factor
        p_res = pres_base * factor
        dt = Constant(dt_base/float(factor))
        num_steps = np.rint(Tend/float(dt))
        
        U_exact = ('-U*exp(-2*nu*pow(pi,2)*t)*(cos(pi*(x[0]))*sin(pi*(x[1])))',
                ' U*exp(-2*nu*pow(pi,2)*t)*(cos(pi*(x[1]))*sin(pi*(x[0])))')
        u_exact = Expression(U_exact, degree=3, U=float(1.), nu=float(nu),t=float(0.))
        P_exact = ('-0.25*exp(-4*nu*pow(pi,2)*t)*(cos(2*pi*(x[0])) + cos(2*pi*(x[1]))-2.0)')
        p_exact = Expression(P_exact, degree = 3, nu=float(nu),t=float(0.))
        
        # Create mesh
        mesh = RectangleMesh(Point(xmin,ymin), Point(xmax,ymax), nx,ny)   

        # Define the ufl-elements 
        W_E    = VectorElement("DG", mesh.ufl_cell(), k)
        T_E    = VectorElement("DG", mesh.ufl_cell(), l)
        Wbar_E = VectorElement("DGT", mesh.ufl_cell(), kbar)

        Q_S    = FiniteElement("DG", mesh.ufl_cell(), k-1)
        Qbar_S = FiniteElement("DGT", mesh.ufl_cell(), k)
               
        # Define spaces 
        # For advection:
        W    = FunctionSpace(mesh,W_E, constrained_domain = PeriodicBoundary(boundary_dict))
        T    = FunctionSpace(mesh,T_E, constrained_domain = PeriodicBoundary(boundary_dict))
        Wbar = FunctionSpace(mesh,Wbar_E, constrained_domain = PeriodicBoundary(boundary_dict) ) 

        # For Stokes
        mixedL = FunctionSpace(mesh, MixedElement([W_E,Q_S]), constrained_domain = PeriodicBoundary(boundary_dict))
        mixedG = FunctionSpace(mesh, MixedElement([Wbar_E,Qbar_S]),constrained_domain = PeriodicBoundary(boundary_dict))
        
        # Define (solution) funcitons
        v0, ustar, duh0, duh00, Udiv  = Function(W), Function(W), \
                                        Function(W), Function(W), Function(W)
        Ubar0, vbarh                  = Function(Wbar), Function(Wbar)
             
        Uh_S, Uh0_S                     = Function(mixedL), Function(mixedL)
        Uhbar_S, Uhbar0_S               = Function(mixedG), Function(mixedG)
                
        # Initialize with exact
        v0.assign(u_exact)
        Ubar0.assign(u_exact)
        assign(Udiv,v0)
        
        # Get forms for PDE-constrained advection, including consistency term
        theta_L         = Constant(1.)
        FuncSpace_adv   = {'FuncSpace_local': W, 'FuncSpace_lambda': T , 'FuncSpace_bar': Wbar}
        forms_adv       = FormsPDEMap(mesh, FuncSpace_adv).forms_theta_nlinear(v0, Ubar0, dt,
                                                                               theta_map, theta_L = theta_L,
                                                                               duh0 = duh0, duh00 = duh00)
        # Get forms for Stokes step
        forms_stokes    = FormsStokes(mesh,mixedL,mixedG,k).forms(ustar,dt,nu,f)
        
        # Define boundary conditions
        bc_S = DirichletBC(mixedG.sub(1), Constant(0), Corner, "pointwise")
        
        # Initialize solver classes
        pde_projection = PDEMap(mesh, forms_adv['N_a'], forms_adv['G_a'], forms_adv['L_a'],
                                                                          forms_adv['H_a'], 
                                                                          forms_adv['B_a'],
                                      forms_adv['Q_a'], forms_adv['R_a'], forms_adv['S_a'])
        stokes_sc      = Stokes_StaticCondensation(mesh, 
                                                   forms_stokes['A_S'], forms_stokes['G_S'],
                                                                        forms_stokes['B_S'], 
                                                   forms_stokes['Q_S'], forms_stokes['S_S'])
        # Preassemble Stokes
        stokes_sc.assemble([bc_S])
        
        # Initialize Lagrangian Particles
        x = RandomRectangle(Point(xmin, ymin), Point(xmax, ymax), seed = 10).generate([p_res, p_res])
        x = comm.bcast(x, root=0)
        nump = len(x)
        periodic = np.array([[xmin,xmin, ymin, ymax],[xmax, xmax, ymin, ymax],\
                             [xmin,xmax, ymin,ymin ],[xmin, xmax, ymax, ymax]])
        lp = LagrangianParticles(W, advection_scheme, periodic)
        lp.add_particles(x, properties_d = {'Velocity': [None]*nump, 'deltaVel': [None]*len(x) })
        lp.mesh_to_particles(v0,'Velocity')
        
        # Compute specific momentum at t = 0
        ux, uy = v0.split()
        xmoment_0, ymoment_0 = assemble(ux*dx), assemble(uy*dx)
        momentum_0= abs(xmoment_0 + ymoment_0)
        if comm.Get_rank() == 0: print('Momentum at time 0 is '+str(momentum_0))
        
        # Time Loop
        step = 0
        while step < num_steps:
            step += 1
            if comm.Get_rank() == 0: print("Step "+str(step)+', time = '+str(float(dt) * step )) 
            
            shift_t, rlocate_t = lp.step(Udiv, dt=float(dt), stepnum = step -1 )
            
            if method is 'pde':
                pde_projection.assemble(lp, 'Velocity')
                pde_projection.solve_global(vbarh)
                pde_projection.backsubstitute(vbarh, ustar)
            elif method is 'lstsq':
                pde_projection.lstsq_map(lp, 'Velocity', ustar)
            else:
                dolfin_error('Projection method not supported')
                        
            # Do Stokes Step (only reassembling of RHS needed)
            stokes_sc.assemble_rhs()
            stokes_sc.solve_global(Uhbar_S)
            stokes_sc.backsubstitute(Uhbar_S, Uh_S)          
            
            # Update for next time step
            assign(v0, ustar)
            assign(Ubar0, Uhbar_S.sub(0))
            assign(Udiv, Uh_S.sub(0))

            assign(duh00, duh0)
            assign(duh0, project(Uh_S.sub(0)-ustar,W))
            
            div_error_ustar  = assemble( div(ustar) * div(ustar) * dx )
            div_error_stokes = assemble( div(Uh_S.sub(0)) * div(Uh_S.sub(0)) * dx )

            # Update particle property
            lp.mesh_to_particles_flip(Uh_S.sub(0), ustar, 
                                       'Velocity', 'deltaVel', step-1, 0.5)
            # Use consistency term
            if step == 2: theta_L.assign(0.5)
                
            # Compute and print error
            u_exact.t = step * float(dt)
            u_error = sqrt(assemble(dot(Uh_S.sub(0) - u_exact, Uh_S.sub(0) - u_exact)*dx) )
            if comm.Get_rank() == 0:
                print 'L2 error', u_error
                
        # When finished, store some results
        outfile_u = File(outdir+"tg_velocity_tend_nx_"+str(nx)+".pvd")
        outfile_p = File(outdir+"tg_pressure_tend_nx_"+str(nx)+".pvd")
        outfile_u  << Uh_S.sub(0)
        outfile_p  << Uh_S.sub(1)
        
        u_exact.t = step * float(dt)
        p_exact.t = step * float(dt)
        
        u_error = sqrt(assemble(dot(Uh_S.sub(0) - u_exact, Uh_S.sub(0) - u_exact)*dx) )
        p_error = sqrt(assemble(dot(Uh_S.sub(1) - p_exact, Uh_S.sub(1) - p_exact)*dx) )
        
        outfile_du = File(outdir+"tg_dvelocity_tend_nx_"+str(nx)+".pvd")
        outfile_dp = File(outdir+"tg_dpressure_tend_nx_"+str(nx)+".pvd")
        
        outfile_du << project(Uh_S.sub(0) - u_exact, W)
        outfile_dp << project(Uh_S.sub(1) - p_exact, FunctionSpace(mesh, 'DG', k-1))
        
        num_cells_p = comm.gather(mesh.num_cells(), root = 0) 
        mesh_hmin   = comm.gather(mesh.hmin(), root = 0)

        # Momentum at t = Tend
        ux, uy = Uh_S.sub(0).split()
        xmoment_end = assemble(ux*dx)
        ymoment_end = assemble(uy*dx)
        momentum_end= abs(xmoment_end + ymoment_end)
        
        if comm.Get_rank() == 0:
            print 'L2 u_error', u_error
            print 'L2 p_error', p_error
            print momentum_0, xmoment_0, ymoment_0
            print momentum_end, xmoment_end, ymoment_end
            print 'Momentum error', float(momentum_end - momentum_0)
            num_cells_t = sum(num_cells_p)
            mesh_hmin_t = min(mesh_hmin)
            num_particles = len(x)
            with open(error_table, "a") as write_file:
                write_file.write("%10.5g & %6d & %10.5g &  %8d & %8.1e  & %8.1e & %8.1e \\\\ \n" % 
                                                            (float(dt), int(num_cells_t), float(mesh_hmin_t),  int(num_particles),
                                                            float(u_error), float(p_error), 
                                                            float(momentum_end - momentum_0)))

